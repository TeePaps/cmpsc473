#include <stdio.h>
#include <stdlib.h>

// Function prototypes
void set_bit(int bit);
int inspect_bit(int bit);
void clear_bit(int bit);
int bit_op(int bit);
void printBit(int bit);
int getMask(int bit);

typedef enum { SET, INSPECT, CLEAR } BIT_OPS;

// Global variables
int SIZE_OF_BITMAP = 3;
int* bitmap = NULL;
BIT_OPS bitOp = SET;

int main() {
  // Initialize the bitmap to 0
  bitmap = calloc( SIZE_OF_BITMAP, sizeof(int) );
  int bit = 10;
  int bitValue;

  bitValue = inspect_bit(10);
  printBit(bit);
  set_bit(10);
  set_bit(60);
  set_bit(78);
  set_bit(19);
  set_bit(120);
  bitValue = inspect_bit(10);
  printBit(bit);
  clear_bit(10);
  bitValue = inspect_bit(10);
  printBit(bit);
}

void set_bit(int bit) {
  bitOp = SET;
  bit_op(bit);
}

int inspect_bit(int bit) {
  bitOp = INSPECT;
  return bit_op(bit);
}

void clear_bit(int bit) {
  bitOp = CLEAR;
  bit_op(bit);
}


int bit_op(int bit) {
  int intOffset = ( bit / sizeof(int) );
  int intBlock = bitmap[intOffset];

  // Do the operation
  if (bitOp == SET) {
    intBlock |= getMask(bit);
  }
  if (bitOp == INSPECT) {
    return( ( intBlock & getMask(bit) ) >> intOffset );
  }
  if (bitOp == CLEAR) {
    intBlock &= ~getMask(bit);
  }

  bitmap[intOffset] |= intBlock;
  return( 0 );
}

int getMask(int bit) {
  int bitOffset = ( bit % sizeof(int) );
  return( 1 << bitOffset );
}

void printBit(int bit) {
  int bitsInByte = 8;
  printf( "\n\nBit %d = %d\n", bit, inspect_bit(bit) );
  printf( "All bits:");
  int i;
  for ( i = 0; i < SIZE_OF_BITMAP * sizeof(int) * bitsInByte; i++ ) {
    if ( ( i % bitsInByte ) == 0 ) {
      printf("\n");
    }
    printf("%d", inspect_bit(i));
  }
}
