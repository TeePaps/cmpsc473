# Homework 2/3/4 #

Author: Ted Papaioannou

-----------------------

## Problem 1 ##
***[8 points]  The experimental Synthesis operating system has an assembler
incorporated within the kernel.  To optimize system-call performance, the kernel
assembles routines within kernel space to minimize the path that the system call
must take through the kernel.  This approach is the antithesis of the layered
approach, in which the path through the kernel is extended to make building the
operating system easier.  Discuss the pros and cons of the Synthesis approach to
kernel design and to system-performance optimization.***

##### Pros of synthesis approach: #####

* Being based on macro data flow for parallel programming ease
  (For high performance)
* kernel code synthesis
* reduced synchronization
* fine grain scheduling
* valued rendering
* Emulation of Guest OS's allowing existing software written for other
  OS's to run with little to no modifications
* Several dozen times times speedup for Unix Kernel calls

##### Cons of synthesis approach: #####

* Inflated kernel size due to code redundancy.
* structuring of kernel and correctness algorithms
* protection of synthesized code


-----------------
## Problem 2 ##
***[10 points]  Direct memory access is used for high-speed I/O devices in order
to avoid increasing the CPU’s execution load.***

a. ***How does the CPU interface with the device to coordinate the transfer?***

    To initiate the transfer, the host writes a **DMA command block** into
    memory. This block contains a pointer to the source of a transfer, a pointer
    to the destination of the transfer, and a count of the number of bytes to be
    transferred. The CPU writes the address of this command block to the DMA
    controller, then goes on with other work. The DMA controller proceeds to
    operate the memory bus directly, placing addresses on the bus to perform
    transfers without the help of the main CPU.

    Then, handshaking between the DMA controller and the device controller is
    performed via a pair of wires called **DMA-request** and **DMA-acknowledge**.
    the device controller palces a signal on the DMA-request wire when a word of
    data is available for transfer. This signla causes the DMA controller to
    seize and place a signal on the DMA-acknowledge wire. When the device
    controller receives the DMA-acknowlege signal, it transfers the word of data
    to memory and removes the DMA-request signal.

b. ***How does the CPU know when the memory operations are complete?***

    When the enitre transfer is finished, the DMA controller interrupts the
    CPU.

c. ***The CPU is allowed to execute other programs while the DMA controller is
  transferring data.  Does this process [the DMA controller's data transfers]
  interfere with the execution of the user programs?  If so, describe what
  forms of interference are caused.***


    Yes, this causes **cycle stealing**, which can slow down CPU computation.
    The CPU is momentarily prevented from accessing main memory, yet it can
    still access data items in its primary and secondary caches.

d. ***Since multicore processors are so common now, what would be the effect of
  using one of the cores as a replacement for a separate DMA controller?***

    In this case, resources would be wasted. The purpose of the DMA controller
    is to avoid burdening an expensive genreal-purpose processor to handle
    **programmed I/O (PIO)**. Although the main CPU is no longer being
    burdened, the core that will act as a seperate DMA controller will still
    waste its potential.

e. ***It's not quite right to say that DMA should be used with "high-speed I/O
  devices".  How else should the class of devices be described, to help explain
  why and when DMA is useful?***

    The DMA should be used with devices that do large transfers, such as disk
    drives. They are useful to reduce wasting general-purpose processing power.
    The DMA controller can be used when large chunks of data must be written
    that may not be required by a process immediately.

*Sources: OSC (pg. 595, 596)*

-----------------
## Problem 3 ##
***[10 points]  Describe the differences among short-term, medium-term, and
long-term scheduling.***

* ***Be sure to consider why and when the scheduling activity occurs, and how
much time passes between activities of the same type (which explains why it's
called short-, medium- and long-term).***
* ***Consider a web server that generates a new process or thread to handle a
new client request.  Does the rate of activity from clients blur the distinction
between short- and long-term?***
* ***Consider a web server that pre-allocates processes or threads to handle new
client requests.  How does this impact the original question?***

##### Short-term: #####
It selects from among the processes that are ready to execute and allocates the
CPU to one of them. It executes at least once every 100 milliseconds. Because of
the short time between executions, the short-term scheduler must be fast.

##### Medium-term: #####
Time sharing systems may introduce this intermediate level of scheduling.
If it is advantageous to remove a process from memory (and from active
contention for the CPU), it uses **swapping** which will reduce the degree of
multiprogramming. Swapping amy be necessary to improve the process mix or
because a change in memory requirements has overcommitted available memory,
requiring memory to be freed up.

##### Long-term: #####
Selects processes from this pool and loads them into memory for execution. It
executes much less frequently; *minutes* may separate the creation of one new
process and the next. It controls the **degree of multiprogramming** (the
number of processes in memory). Thus, it may need to be invoked only when a
process leaves the system. It can afford to take more time to decide which
process should be selected for execution. It is important for it to select a
good ***process mix*** of I/o-bound and CPU-bound processes. Time sharing
systems ofteen have no long-term scheduler, only short-term.


##### Considerations: #####
Considering a web server that generates a new process or thread to handle a new
client request, the rate of client activity does in fact blur the distinction
between short- and long-term. Because the rate is largely variable and unknown
to the system, the long-term scheduler will have trouble making accurate
desicisions.
<p>
If the web server instead *pre-allocates* processes or threads to handle new
requests, the distintion will be clearer. The long-term scheduler will now be
able to more accurately determine how to multiprogram much more efficiently.
The short-term scheduler will still be able to perform the same.


*Sources: OSC (pg. 112, 113)*

-----------------
## Problem 4 ##
***[10 points]  Consider the following set of processes, with the length of the
CPU burst given in milliseconds:***

| Process | Burst Time | Priority |
|:-------:|:----------:|:--------:|
|   P1    |      8     |    3     |
|   P2    |      3     |    1     |
|   P3    |      2     |    3     |
|   P4    |      3     |    4     |
|   P5    |      5     |    2     |

a. ***Draw four Gantt charts that illustrate the execution of these processes
  using the following scheduling algorithms:  First-Come-First-Served, Shortest
  Job First, nonpreemptive priority (a smaller priority number implies a higher
  priority), and Round Robin (time quantum = 1 millisecond).***


    FCFS

    ![FCFS](4.a-FCFS.png)

    SJF

    ![SJF](4.a-SJF.png)

    NP

    ![NP](4.a-NP.png)

    RR

    ![RR](4.a-RR.png)

b. ***What is the turnaround time of each process for each of the scheduling
  algorithms in part a?***

    | Process | FCFS | SJF | NP | RR |
    |:-------:|:----:|:---:|:--:|:--:|
    |   P1    |  8   |  21 | 16 | 21 |
    |   P2    |  11  |  5  | 3  | 12 |
    |   P3    |  13  |  2  | 18 | 8  |
    |   P4    |  16  |  8  | 21 | 13 |
    |   P5    |  21  |  13 | 8  | 18 |

c. ***What is the waiting time of each process for each of these scheduling
  algorithms?***

    | Process | FCFS | SJF | NP | RR |
    |:-------:|:----:|:---:|:--:|:--:|
    |   P1    |  0   |  13 | 8  | 13 |
    |   P2    |  8   |  2  | 0  | 9  |
    |   P3    |  11  |  0  | 16 | 6  |
    |   P4    |  13  |  5  | 18 | 10 |
    |   P5    |  16  |  8  | 3  | 13 |

d. ***Which of the algorithms results in the minimum average waiting time (over
  all processes)?***

    | Algorithm | Total Waiting Time | Average Waiting Time |
    |:---------:| ------------------:|:--------------------:|
    |    FCFS   |  0+8+11+13+16 = 48 |          9.6         |
    |  **SJF**  | **13+2+0+5+8 = 28**|        **5.6**       |
    |    NP     |  8+0+16+18+3 = 47  |          9.0         |
    |    RR     |  13+9+6+10+13 = 51 |         10.2         |

    *Shortest Job First* has the shortest waiting time.


-----------------
## Problem 5 ##

***[10 points]  Consider a variant of the Round Robin scheduling algorithm in
which the entries in the ready queue are pointers to the Process Control
Blocks.***

a. ***What would be the effect of putting two pointers to the same process in
  the ready queue?***
  That process would be run twice as often, thus its turnaround time would be
  at least half.

b. ***What would be two major advantages and two disadvantages of this
  scheme?***

    Advantages:
    1. A process could gain a higer priority simply by its pointer being added to
       queue multiple times.
    2. The turnaround time of a specific process would be less.

    Disadvantages:
    1. A process, possibly rogue, could game the system to gain a higher
       priority that it should actually have, taking up excessive CPU time.
    2. Both the waiting time and turnaround time for other process would
       increase.

c. ***How would you modify the basic Round Robin algorithm to achieve the same
  effect without the duplicate pointers?***

    One simple way would be to add an option to the process that notifies the
scheduler to reschedule the process a certain number of time quantums after it
has finished. A bit is set in the process to signal that it is going to rerun.
Once the process has finished rerunning, the scheduler checks the bit to see if
it should rerun the process again after certain number of time quantums. If
that bit is set, it will not rerun that process again until it's originally
scheduled time slot.

-----------------
## Problem 6 ##

***[12 points]  The Cigarette-Smokers Problem.  Consider a system with three
smoker processes and one agent process.  Each smoker continuously rolls a
cigarette and then smokes it.  But to roll and smoke a cigarette, the smoker
needs three ingredients: tobacco, paper, and matches.  One of the smoker
processes has paper, another has tobacco, and the third has matches.  The agent
has an inﬁnite supply of all three materials.  The agent places two of the
ingredients on the table.  The smoker who has the remaining ingredient then
makes and smokes a cigarette, signaling the agent on completion.  The agent then
puts out another two of the three ingredients, and the cycle repeats.***

    VAR
    tobacco_paper, tobacco_matches, paper_matches, done_smoking: SEMAPHORE;

    PROCEDURE agent
    BEGIN
      WHILE (making_money) DO
      BEGIN
        CASE (random(1,3)) OF
          1: signal(tobacco_paper);
          2: signal(tobacco_matches);
          3: signal(paper_matches);
        END;
        wait(done_smoking);
      END;
    END;

    PROCEDURE smoker1   { This smoker has matches }
    BEGIN
      WHILE (not_dead) DO
      BEGIN
        wait(tobacco_paper);
          smoke;
        signal(done_smoking);
      END;
    END;

    PROCEDURE smoker2   { This smoker has paper }
    BEGIN
      WHILE (addicted_to_nicotine) DO
      BEGIN
        wait(tobacco_matches);
          smoke;
        signal(done_smoking);
      END;
    END;
    PROCEDURE smoker3   { This smoker has tobacco }
    BEGIN
      WHILE (can_inhale) DO
      BEGIN
        wait(paper_matches);
          smoke;
        signal(done_smoking);
      END;
    END;

    BEGIN
    tobacco_paper := 0;
    tobacco_matches := 0;
    paper_matches := 0;
    done_smoking := 0;

    END.

a. ***What would be the effect of using two semaphores instead of one, in each
case?  That is, replace signal(tobacco_paper); with signal(tobacco);
signal(paper); and so on.  You could explain why the modified code is still
correct, or give an example to show why it is now incorrect.***

    Two semaphores would not work properly due to a race case condition which
    would lead to a deadlock. Take for example the case when the agent calls
    ```signal(tobaco); signal(matches)```, smoker1 calls ```wait(tobaco);
    wait(paper)```, and smoker2 calls ```wait(tobaco); wait(matches)```. After
    the agent calls ```signal(tobaco)```, there will be a race case on
    ```wait(tobaco)```. If smoker1 gets past ```wait(tobaco)``` first, then
    it will call ```wait(paper)```. But because smoker2 is waiting on  'tobaco',
    ```signal(done_smoking)``` will not be called by either smoker, thus the
    agent will never be able to call ```signal(paper)```. Therefore, a deadlock
    will occur.


b. ***How should the code be modified to handle more than three smokers, or more
than one agent, or more than three tobacco-related products?  [No illegal
substances, please.]***

    * More than 3 smokers:

        The code would not have to be modified. Even though a race case will
        occur for who gets past ```wait(product_otherproduct)```, that race case
        will not cause any deadlock. 

    * More than one agent:
        Each semaphore would instead need to be instantiated to (num_of_agents
        -1) to account for the additional products that each agent holds.

    * More than 3 tobaco related products
        The simplest way would be to add a new semaphore for each pair so that
        there are (num_of_products - 1) semaphores in total.


c. ***What would be the effect of moving the agent's statement
wait(done_smoking); to before the CASE statement?  What else would need to be
changed as a result of this move?***

    This would cause a deadlock since agent would be wait for a signal that will
    never come. To account for this, done_smoking would need to be initialized
    to 1 so that it must be called twice without a signal before it would
    properly stop halt agent's execution.
