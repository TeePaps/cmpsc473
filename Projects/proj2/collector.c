/* collector
 */

/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include "collector.h"
#include "pthread_wrappers.h"
#include "utils.h"
#include "buffer.h"


void init_collector(struct collector *c, int fd, struct buffer* buffer) {
  if (c == NULL) {
    return;
  }

  // p->tid is set via pthread_create() in main()
  // p->exit_status is set via pthread_join() in main()
  c->tid = (pthread_t)-1;
  c->fd = fd;
  c->buffer = buffer;
  c->line_list = NULL;
  c->exit_status = (void *)-1;
}

void print_collector(struct collector *collector) {
  if (collector == NULL) {
    printf("producer at NULL\n");
  }
  else {
    /* printf("producer at %p\n", (void *)collector); */
    /* printf("  thread id = 0x%jx\n", (uintmax_t)collector->tid); */
    /* printf("  file descriptor = 0x%d\n", (uintmax_t)collector->fd); */
    /* printf("  all buffers = %p\n", (void *)collectors->bufs); */
    /* printf("  exit status = %p\n", collector->exit_status); */
  }
}

void * collector_func(void * arg) {
  struct collector *collector = (struct collector *)arg;

  if (collector == NULL) {
    printf("collector_func(), thread 0x%jx, NULL arg\n", (uintmax_t)pthread_self());
    exit(1);
  }

  // see parent_actions() for similar code, with comments
  FILE *fp = fdopen(collector->fd, "w");
  if (fp == NULL) {
    err_sys("fdopen(w) error");
  }

  static char fp_buffer[BUFFER_SIZE];
  int retr = setvbuf(fp, fp_buffer, _IOLBF, BUFFER_SIZE);
  if ( retr != 0 ) {
    err_sys("setvbuf error (p2)");
  }

  struct item line;
  struct node* head = NULL;
  while (1) {
    // only lines that matched are in buffer
     line = remove_buffer(collector->buffer);

     if (line.type == ITEM_TYPE_VALID) {
      // send line if not already sent
      int item_sent = find_val(head, line.string);
      if (item_sent < 0) {
        // send the line to the pipe
        fprintf(fp, "%s", line.string);

        // save in list
        insert_val(&head, line.string);
      }
    }
    else if (line.type == ITEM_TYPE_TERMINATE) {
      break;
    }
    else {
      printf("bogus type in the queue");
      exit(1);
    }
  }

  // Clean up the FILE* and file descriptor
  fclose(fp);
  fp = NULL;
  close(collector->fd);
  collector->fd = -1;

  return arg;
}
