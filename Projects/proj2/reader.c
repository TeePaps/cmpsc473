/* Reader
 */

/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>

#include "buffer.h"
#include "utils.h"
#include "reader.h"

void init_reader(struct reader* reader, int fd, struct buffer** bufs, int mArgc)
{
  if (reader == NULL) {
    return;
  }

  // reader->tid is set via pthread_create() in main()
  // reader->exit_status is set via pthread_join() in main()
  reader->tid = (pthread_t)-1;
  reader->fd = fd;
  reader->buffers = bufs;
  reader->mArgc = mArgc;
  reader->exit_status = (void *)-1;
}

void print_reader(struct reader *reader) {
  if (reader == NULL) {
    printf("producer at NULL\n");
  }
  else {
    printf("producer at %p\n", (void *)reader);
    printf("  thread id = 0x%jx\n", (uintmax_t)reader->tid);
    printf("  file descriptor = %d\n", reader->fd);
    printf("  all buffers = %p\n", (void *)reader->buffers);
    printf("  exit status = %p\n", reader->exit_status);
  }
}

void * reader_func(void * arg) {
  struct reader *reader = (struct reader *)arg;

  if (reader == NULL) {
    printf("reader_func(), thread 0x%jx, NULL arg\n", (uintmax_t)pthread_self());
    exit(1);
  }

  // see parent_actions() for similar code, with comments
  FILE *fp = fdopen(reader->fd, "r");
  if (fp == NULL) {
    err_sys("fdopen(r) error");
  }

  static char fp_buffer[BUFFER_SIZE];
  int retr = setvbuf(fp, fp_buffer, _IOLBF, BUFFER_SIZE);
  if ( retr != 0 ) {
    err_sys("setvbuf error (p2)");
  }

  char* line = malloc(BUFFER_SIZE * sizeof(char));
  char *p;                  // String to copy read line into

  // Insert all the lines coming off the pipe into all of the buffers
  while ((p = fgets(line, BUFFER_SIZE, fp)) != NULL) {
    int i;
    struct item item = { .type = ITEM_TYPE_VALID, .string = line };
    for (i = 0; i < reader->mArgc; i++) {
      insert_buffer(reader->buffers[i], &item);
    }

    // allocate memory for next line
    line = malloc(BUFFER_SIZE * sizeof(char));
  }

  // free the memeory from the last unused line
  free(line);
  line = NULL;

  // Clean up the FILE* and file descriptor
  fclose(fp);
  fp = NULL;
  close(reader->fd);
  reader->fd = -1;
}
