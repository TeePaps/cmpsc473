#ifndef UTILS_H
#define UTILS_H

// Defines
#define BUFFER_SIZE 4096
#define ARGUMENTS "i:o:m:"
#define DEFAULT_INPUT "/dev/null"
#define DEFAULT_OUTPUT "/dev/null"

// Global Variables
struct arg_struct { // Holds parsed arguments and meta data about them
  char* inFile;     // Input file to read from
  char* outFile;    // Output file to write to
  int mArgc;        // Count of -m arguments
  char** mArgv;     // Array of -m arguments
} arguments;

// Structs
struct node {
  char* val;
  struct node* next;;
};

////////////////////////////////////////////////////////////////////////////////
//
// Function     : insert_node
// Description  : inserts a node into a linked list
//
// Inputs       : head - head node of the linked list
//                new_node- node to insert
// Outputs      : None

void insert_val(struct node** head, char* val);

////////////////////////////////////////////////////////////////////////////////
//
// Function     : find_val
// Description  : find value in a linked list
//
// Inputs       : head - head node of the linked list
//                val - value to search for
// Outputs      : 0 if found, -1 if not

int find_val(struct node* head, const char* val);

////////////////////////////////////////////////////////////////////////////////
//
// Function     : err_sys
// Description  : print message and quit
//
// Inputs       : msg - the error message
// Outputs      : None

void err_sys(char* msg);

////////////////////////////////////////////////////////////////////////////////
//
// Function     : cleanUp
// Description  : Cleans up the allocated memory
//
// Inputs       : None
// Outputs      : None

void cleanUp();

////////////////////////////////////////////////////////////////////////////////
//
// Function     : parseArgs
// Description  : parse the command line arguments into the global arguments
//                struct
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int parseArgs(int argc, char* argv[]);

#endif
