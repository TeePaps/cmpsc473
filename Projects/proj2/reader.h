#ifndef READER_H
#define READER_H

/* reader from pipe
 */

/*----------------------------------------------------------------------*/
// reader

#include "buffer.h"
#include "pthread_wrappers.h"

struct reader {
  /* identifiers */
  pthread_t tid;      /* set via pthread_create() */
  /* input */
  int fd;
  struct buffer** buffers;
  int mArgc;
  char* mArgv;
  void *exit_status;  /* set via pthread_join() */
};

void init_reader(struct reader *r, int fd, struct buffer** buffs, int mArgc);
void print_reader(struct reader *reader);
void * reader_func(void * arg);

#endif
