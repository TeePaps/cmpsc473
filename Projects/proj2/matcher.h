#ifndef MATCHER_H
#define MATCHER_H

/* reader from pipe
 */

#include "pthread_wrappers.h"
#include "buffer.h"

struct matcher {
  /* identifiers */
  pthread_t tid;              /* set via pthread_create() */
  /* input */
  struct buffer* line_queue;
  struct buffer* line_out;
  char* arg;                  // String to search for
  int line_count;             // Number of lines 'string' matched for
  int match_count;            // Total matches on 'string'
  void *exit_status;          /* set via pthread_join() */
};

void init_matcher(struct matcher* matcher, struct buffer* line_queue,
    struct buffer* line_out, char* string);
void print_matcher(struct matcher *matcher);
void * matcher_func(void * arg);

#endif
