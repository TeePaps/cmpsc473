/* CMPSC 473, Project 2
 *
 * FILE:            pr2.c
 * AUTHOR:          Ted Papaioannou
 * EMAIL:           tap5199@psu.edu
 * PSU ACCESS ID:   984238848
 * SUBMITTED ON:    3/8/2014
 */

//------------------------------------------------------------------------------

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
// This makes Solaris and Linux happy about waitpid(); it is not required on Mac OS X.
#include <sys/wait.h>

#include "buffer.h"
#include "reader.h"
#include "matcher.h"
#include "collector.h"
#include "utils.h"
#include "pthread_wrappers.h"
#include "sem_wrappers.h"

#include <inttypes.h>

// Functional prototypes
void p1_actions(int fd);                // write to fd
void p2_actions(int fdr, int fdw);      // read from fdr, write to fdw
void p3_actions(int fd);                // read from fd

////////////////////////////////////////////////////////////////////////////////
//
// Function     : main
// Description  : The main function for the Project 2
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int main(int argc, char *argv[]) {

  int fd12[2];        // pipe from p1 to p2 endpoints
  int fd23[2];        // pipe from p2 to p3 endpoints
  pid_t child1_pid, child2_pid;

  // Parse the command line arguments into the global arguments struct
  if (parseArgs(argc, argv) < 0) {
    err_sys("argument parsing error");
  }

  // Create pipe from p1 to p2
  if (pipe(fd12) < 0) {
    err_sys("pipe 1 to 2 error");
  }
  // Create pipe from p2 to p3
  if (pipe(fd23) < 0) {
    err_sys("pipe 2 to 3 error");
  }

  // Create a child process for p2
  if ((child1_pid = fork()) < 0) {
    err_sys("fork error");
  }
  else if (child1_pid > 0) {  // this is p1 and the parent
    close(fd23[0]);       // Unused pipe
    close(fd23[1]);       // Unused pipe
    close(fd12[0]);       // read from fd[0]
    p1_actions(fd12[1]);  // write to fd[1]

    if (waitpid(child1_pid, NULL, 0) < 0) {  // wait for child
      err_sys("waitpid 1 error");
    }
  }
  else {  // this is p2
    // Create a child process for p3
    if ((child2_pid = fork()) < 0) {
      err_sys("fork error");
    }
    else if (child2_pid > 0) {  // this is p2 and the parent of p3
      close(fd12[1]);                 // write to fd12[1]
      close(fd23[0]);                 // read from fd23[0]
      p2_actions(fd12[0], fd23[1]);   // read from fd12[0], write to fd23[1]

      if (waitpid(child2_pid, NULL, 0) < 0) {  // wait for child
        err_sys("waitpid 2 error");
      }
    }
    else { // this is p3
      close(fd12[0]);       // Unused pipe
      close(fd12[1]);       // Unused pipe
      close(fd23[1]);       // write to fd23[1]
      p3_actions(fd23[0]);  // read from fd23[0]
    }
  }

  cleanUp();
  return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p1_actions
// Description  : read from file, write to fd
//
// Inputs       : fd - the file descriptor to write to
// Outputs      : None

void p1_actions(int fd) {

  char line[BUFFER_SIZE];
  char* p;
  int bytesRead = 0;

  // Check if input file was supplied
  if (arguments.inFile == NULL) {
     err_sys("No input file supplied");
  }

  // Open the input file
  FILE* in = fopen(arguments.inFile, "r");
  if (in == NULL) {
    err_sys("fopen(r) error");
  }

  // Open the output file for the pipe
  FILE* fp = fdopen(fd, "w");     // use fp as if it had come from fopen()
  if (fp == NULL) {
    err_sys("fdopen(w) error");
  }

  // The following is so we don't need to call fflush(fp) after each fprintf().
  // The default for a pipe-opened stream is full buffering, so we switch to line
  //   buffering.
  // But, we need to be careful not to exceed BUFFER_SIZE characters per output
  //   line, including the newline and null terminator.
  static char buffer[BUFFER_SIZE];  // off the stack, always allocated
  int ret = setvbuf(fp, buffer, _IOLBF, BUFFER_SIZE); // set fp to line-buffering
  if (ret != 0) {
    err_sys("setvbuf error (p1)");
  }

  // Put each line of the file onto the pipe
  while ((p = fgets(line, BUFFER_SIZE, in)) != NULL) {
    bytesRead += strlen(p);
    fprintf(fp, "%s", line);
  }

  // Print the results
  printf("P1: file %s, bytes %d\n", arguments.inFile, bytesRead);

  // Clean up the file stream and the file descriptor
  fclose(fp);
  close(fd);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p2_actions
// Description  : read from fdr, write to fdw
//
// Inputs       : fdr - the file descriptor to read from
//                fdw - the file descriptor to write to
// Outputs      : None

void p2_actions(int fdr, int fdw) {
  struct buffer* all_buffers[arguments.mArgc];
  struct buffer* collect_buf;
  struct matcher matchers[arguments.mArgc];
  struct collector collector;
  struct reader reader;
  /* struct collector collector; */

  // create the search threads and buffers
  int i;
  collect_buf = allocate_buffer(arguments.mArgc);
  for (i = 0; i < arguments.mArgc; i++) {
    // allocate a new buffer for each search thread
    all_buffers[i] = allocate_buffer(1);  // test with queue size of 1

    // start search/matcher (consumer) threads
    init_matcher(&matchers[i], all_buffers[i], collect_buf, arguments.mArgv[i]);
    Pthread_create(&matchers[i].tid, NULL, matcher_func, (void *)&matchers[i],
      "p2_actions() matcher");
  }

  // start the collector thread
  init_collector(&collector, fdw, collect_buf);
  Pthread_create(&collector.tid, NULL, collector_func, (void *)&collector,
    "p2_actions() collector");

  // start reader (producer) thread
  init_reader(&reader, fdr, all_buffers, arguments.mArgc);
  Pthread_create(&reader.tid, NULL, reader_func, (void *)&reader,
    "p2_actions() reader");

  // wait for reader
  Pthread_join(reader.tid, &reader.exit_status, "p2_actions() reader");

  // stop matchers and collector
  struct item terminate = { .type = ITEM_TYPE_TERMINATE };
  for (i = 0; i < arguments.mArgc; i++) {
    insert_buffer(all_buffers[i], &terminate);
  }
  insert_buffer(collect_buf, &terminate);

  // wait for matchers and collector
  for (i = 0; i < arguments.mArgc; i++) {
    Pthread_join(matchers[i].tid, &matchers[i].exit_status, "p2_actions() matcher");
  }
  Pthread_join(collector.tid, &collector.exit_status, "p2_actions() collector");

  // deallocate the created buffers
  for (i = 0; i < arguments.mArgc; i++) {
    deallocate_buffer(all_buffers[i]);
  }
  deallocate_buffer(collect_buf);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p3_actions
// Description  : read from fd
//
// Inputs       : fd - the file descriptor to read from
// Outputs      : None

void p3_actions(int fd) {
  // see p1_actions() for similar code, with comments
  FILE *fp = fdopen(fd, "r");
  if (fp == NULL) {
    err_sys("fdopen(r) error");
  }

  // Open the output file
  FILE* out = fopen(arguments.outFile, "w");
  if (out == NULL) {
    out = stdout;
  }

  static char buffer[BUFFER_SIZE];
  int ret = setvbuf(fp, buffer, _IOLBF, BUFFER_SIZE);
  if (ret != 0) {
    err_sys("setvbuf error (child)");
  }

  char line[BUFFER_SIZE];

  char *p;
  int lineCount = 0;
  while ((p = fgets(line, BUFFER_SIZE, fp)) != NULL) {
    fprintf(out, "%s", line);
    lineCount++;
  }

  // Print the results
  printf("P3: file %s lines %d\n", arguments.outFile, lineCount);

  // Clean up the file streams and file handles
  fclose(fp);
  fclose(out);
  close(fd);
}

//--------------------------------------------------------------------------------
