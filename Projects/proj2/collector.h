#ifndef COLLECTOR_H
#define COLLECTOR_H

#include <pthread.h>

/* reader from pipe
 */

/*----------------------------------------------------------------------*/
// reader

struct collector {
  /* identifiers */
  pthread_t tid;      /* set via pthread_create() */
  /* input */
  int fd;
  struct buffer* buffer;
  struct node* line_list;
  void *exit_status;  /* set via pthread_join() */
};

void init_collector(struct collector *p, int fd, struct buffer* buf);
void print_collector(struct collector *p);
void * collector_func(void * arg);

#endif
