/* searcher
 */

/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include "matcher.h"

void init_matcher(struct matcher *matcher, struct buffer* line_queue,
    struct buffer* line_out, char* arg) {
  if (matcher == NULL) {
    return;
  }

  // p->tid is set via pthread_create() in main()
  // p->exit_status is set via pthread_join() in main()
  matcher->tid          = (pthread_t)-1;
  matcher->line_queue   = line_queue;
  matcher->line_out     = line_out;
  matcher->arg          = arg;
  matcher->line_count   = 0;
  matcher->match_count  = 0;
  matcher->exit_status  = (void *)-1;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : print_matcher
// Description  : wrapper function for printing a matcher struct
//
// Inputs       : matcher - the struct to print
// Outputs      : None

void print_matcher(struct matcher *matcher) {
  if (matcher == NULL) {
    printf("matcher at NULL\n");
  }
  else {
    printf("matcher at %p\n", (void *)matcher);
    printf("  thread id = 0x%jx\n", (uintmax_t)matcher->tid);
    printf("  search string = %s\n", matcher->arg);
    printf("  line queue read = %p\n", (void *)matcher->line_queue);
    printf("  lines matched = %d\n", (void *)matcher->line_count);
    printf("  matches found = %d\n", (void *)matcher->match_count);
    printf("  exit status = %p\n", matcher->exit_status);
  }
}

///////////////////////////////////////////////////////////////////////////////
// Function     : matcher_func
// Description  : match a line against a -m argument matcher struct supplied.
//                Increments the match count and line count in the struct if
//                necessary.
//
// Inputs       : arg - a matcher struct
// Outputs      : the matcher struct, editted

void * matcher_func(void * arg) {
  struct matcher *matcher = (struct matcher *)arg;

  if (matcher == NULL) {
    printf("matcher_func(), thread 0x%jx, NULL arg\n", (uintmax_t)pthread_self());
    exit(1);
  }

  struct item line;

  // try to match while item in buffer is valid
  while (1) {
    line = remove_buffer(matcher->line_queue);
    if (line.type == ITEM_TYPE_VALID) {
      int count = 0;

      // strstr(char*, char*) returns a pointer to the beginning of the first
      // occurences of the string to search for. Therefore, we must move the start
      // of the pointer pass this point, after each call of strstr, to find the next
      // occurence.
      char* copy = strdup(line.string);
      while((copy = strstr(copy, matcher->arg)) != NULL) {
        copy += strlen(matcher->arg);
        count++;
        matcher->match_count++;
      }

      // Commit the results
      if (count > 0) {
        matcher->line_count++;           // A line has been matched
        struct item * line_to_pass = malloc(sizeof(struct item));
        memcpy(line_to_pass, &line, sizeof(struct item));
        insert_buffer(matcher->line_out, line_to_pass);
      }
    }
    else if (line.type == ITEM_TYPE_TERMINATE) {
      printf("P2: string %s, lines %d, matches %d\n",
        matcher->arg, matcher->line_count, matcher->match_count);
      break;
    }
    else {
      printf("bogus type in the line queue");
      exit(1);
    }
  }

  return arg;
}
