#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "utils.h"

////////////////////////////////////////////////////////////////////////////////
//
// Function     : insert_val
// Description  : inserts a node into a linked list
//
// Inputs       : head - head node of the linked list
//                val - val to insert
// Outputs      : None

void insert_val(struct node** head, char* val) {
  struct node *temp;
  temp=(struct node *)malloc(sizeof(struct node));
  temp->val=val;
  if (head== NULL) {
    (*head)=temp;
    (*head)->next=NULL;
  }
  else {
    temp->next=(*head);
    (*head)=temp;
  }
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : find_val
// Description  : find value in a linked list
//
// Inputs       : head - head node of the linked list
//                val - value to search for
// Outputs      : 0 if found, -1 if not

int find_val(struct node* head, const char* val) {

  while (head != NULL) {
    if (head->val == val) {
      return( 0 );
    }
    head = head->next;
  }
  return( -1 );
}


////////////////////////////////////////////////////////////////////////////////
//
// Function     : err_sys
// Description  : print message and quit
//
// Inputs       : msg - the error message
// Outputs      : None

void err_sys(char* msg) {
  printf("error: PID %d, %s\n", getpid(), msg);
  exit(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : cleanUp
// Description  : Cleans up the allocated memory
//
// Inputs       : None
// Outputs      : None

void cleanUp() {
  free(arguments.mArgv);
  arguments.mArgv = NULL;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : parseArgs
// Description  : parse the command line arguments into the global arguments
//                struct
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int parseArgs(int argc, char* argv[]) {
  int ch;

  // Initialize the arguments
  arguments.inFile = NULL;
  arguments.outFile = NULL;
  arguments.mArgc = 0;

  // Collect the input file, output file, and count of -m args
  while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
    switch(ch) {
    case 'i': // Set input file
      arguments.inFile = optarg;
      break;
    case 'm': // Count the -m args
      arguments.mArgc++;
      break;
    case 'o': // Set output file
      arguments.outFile = optarg;
      break;
    default:
      break;
    }
  }

  // Check only the input file, output defaults to stdin
  if (arguments.inFile == NULL) {
    arguments.inFile = DEFAULT_INPUT;
  }
  if (arguments.outFile == NULL) {
    arguments.outFile = DEFAULT_OUTPUT;
  }

  // Check if any -m arguments were supplied
  if (arguments.mArgc == 0) {
    fprintf(stderr, "warning: no -m option was supplied\n");
  }
  else {
    // Loop through again and collect the -m arguments
    optind = 1; // Reset the index for getopt
    arguments.mArgv = malloc(arguments.mArgc * sizeof(char*));
    int mInd = 0;
    while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
      if (ch == 'm') {
        arguments.mArgv[mInd++] = optarg;
      }
    }
  }

  return( 0 );
}


