#ifndef BUFFER_H
#define BUFFER_H

#include <semaphore.h>

enum item_type {
  ITEM_TYPE_INVALID, ITEM_TYPE_VALID, ITEM_TYPE_TERMINATE
};

struct item {   // print request
  enum item_type type;
  char* string;
};

struct buffer {
  struct item* array;
  int size, items, in, out;
  sem_t mutex, full, empty;
};

void print_item(char* indent, struct item* v);
void print_buffer(struct buffer *b);
struct buffer * allocate_buffer(int N);
void deallocate_buffer(struct buffer *b);
void insert_buffer(struct buffer *b, struct item* item);
struct item remove_buffer(struct buffer *b);
void test_buffer(int size);

#endif
