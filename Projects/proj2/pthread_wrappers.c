/* Pthread wrappers
 */

/*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>

#include "pthread_wrappers.h"

int Pthread_debug = 0;  /* 0 = no extra printing, non-zero = extra printing */

/*----------------------------------------------------------------------------*/

/* These functions do not fail, or do not return, so no wrapper is provided.
 *   pthread_self()
 *   pthread_equal()
 *   pthread_exit()
 * These function were not needed (so far), so no wrapper is provided (yet).
 *   pthread_cancel()
 *   pthread_detach()
 *   pthread_attr_init()
 *   etc.
 */

void Pthread_create(pthread_t *restrict thread,
                   const pthread_attr_t *restrict attr,
                   void *(*start_routine)(void*),
                   void *restrict arg,
                   char *msg)
{
  int ret;

  if (Pthread_debug)
    {
      printf("Pthread_create: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = pthread_create(thread, attr, start_routine, arg);

  if (ret != 0)
    {
      printf("pthread_create: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(ret));
      exit(1);
    }

  if (Pthread_debug)
    {
      printf("Pthread_create: thread 0x%jx, %s: created 0x%jx\n",
        (uintmax_t)pthread_self(), msg, (uintmax_t)*thread);
    }
}

void Pthread_join(pthread_t thread, void **status, char *msg)
{
  int ret;

  if (Pthread_debug)
    {
      printf("Pthread_join: thread 0x%jx, with thread 0x%jx, %s, starting\n",
        (uintmax_t)pthread_self(), (uintmax_t)thread, msg);
    }

  ret = pthread_join(thread, status);

  if (ret != 0)
    {
      printf("pthread_join: thread 0x%jx, %s: failed: %s\n",
            (uintmax_t)pthread_self(), msg, strerror(ret));
      exit(1);
    }

  if (Pthread_debug)
    {
      printf("Pthread_join: thread 0x%jx, with thread 0x%jx, %s, finished\n",
        (uintmax_t)pthread_self(), (uintmax_t)thread, msg);
    }
}

void Pthread_cancel(pthread_t target_thread, char *msg)
{
}

void Pthread_detach(pthread_t thread, char *msg)
{
}

/*----------------------------------------------------------------------------*/
