#ifndef SEM_WRAPPERS_H
#define SEM_WRAPPERS_H

/* semaphore wrappers
 */

/*----------------------------------------------------------------------------*/

#include <semaphore.h>

extern int Sem_debug;   /* 0 = no extra printing, non-zero = extra printing */

/*----------------------------------------------------------------------------*/

void Sem_init(sem_t *sem, int pshared, unsigned int value, char *msg);
void Sem_destroy(sem_t *sem, char *msg);
void Sem_wait(sem_t *sem, char *msg);
void Sem_post(sem_t *sem, char *msg);
void Sem_getvalue(sem_t *sem, int *sval, char *msg);

/*----------------------------------------------------------------------------*/

#endif
