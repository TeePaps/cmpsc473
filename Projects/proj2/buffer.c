#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <errno.h>

#include "sem_wrappers.h"
#include "buffer.h"

#include <pthread.h>

void print_item(char *indent, struct item *v) {

  if (indent == NULL) indent = "";

  if (v == NULL) {
    printf("%sitem at NULL\n", indent);
  }
  else if (v->type == ITEM_TYPE_INVALID) {
    printf("%sitem at %p invalid\n", indent, (void *)v);
  }
  else if (v->type == ITEM_TYPE_VALID) {
    printf("%sitem at %p valid: %s\n", indent, (void *)v, v->string);
  }
  else if (v->type == ITEM_TYPE_TERMINATE) {
    printf("%sitem at %p terminate\n", indent, (void *)v);
  }
  else {
    printf("%sitem at %p bogus\n", indent, (void *)v);
  }
}


void print_buffer(struct buffer *b)
{
  int mutex_val, full_val, empty_val;

  if (b == NULL) {
    printf("buffer at NULL\n");
  }
  else {
    if (b->array == NULL) {
      printf("buffer at %p, no array\n", (void *)b);
    }
    else {
      printf("buffer at %p\n", (void *)b);
      for (int index = b->out; index != b->in; index = (index+1)%b->size) {
        print_item("  ", &b->array[index]);
      }
    }
    printf("  size = %d, items = %d, in = %d, out = %d\n",
      b->size, b->items, b->in, b->out);
    Sem_getvalue(&b->mutex, &mutex_val, "print_buffer() mutex");
    Sem_getvalue(&b->full , &full_val , "print_buffer() full ");
    Sem_getvalue(&b->empty, &empty_val, "print_buffer() empty");
    printf("  mutex val = %d, full val = %d, empty val = %d\n",
      mutex_val, full_val, empty_val);
  }
}

struct buffer * allocate_buffer(int N) {
  struct buffer *b = malloc(sizeof(struct buffer));

  if (b == NULL) {
    printf("allocate_buffer: thread 0x%jx, failed (1): %s\n",
      (uintmax_t)pthread_self(), strerror(errno));
    exit(1);  // return NULL;
  }

  b->array = malloc(N*sizeof(struct item));

  if (b->array == NULL) {
    printf("allocate_buffer: thread 0x%jx, failed (2): %s\n",
      (uintmax_t)pthread_self(), strerror(errno));
    free(b);
    exit(1);  // return NULL;
  }

  b->size = N;
  b->items = 0;
  b->in = 0;
  b->out = 0;

  Sem_init(&b->mutex, 0, 1, "allocate_buffer() mutex");
  Sem_init(&b->full , 0, 0, "allocate_buffer() full ");
  Sem_init(&b->empty, 0, N, "allocate_buffer() empty");

  return b;
}

void deallocate_buffer(struct buffer *b) {
  if (b == NULL) {
    return;
  }

  Sem_destroy(&b->mutex, "deallocate_buffer() mutex");
  Sem_destroy(&b->full , "deallocate_buffer() full ");
  Sem_destroy(&b->empty, "deallocate_buffer() empty");

  free(b->array);
  free(b);
}

void insert_buffer(struct buffer *b, struct item* item) {
  if (b == NULL || b->array == NULL || item == NULL) {
    return;
  }

  Sem_wait(&b->empty, "insert_buffer() empty");
  Sem_wait(&b->mutex, "insert_buffer() mutex");
  b->array[b->in] = *item;
  b->in = (b->in + 1) % b->size;
  b->items++;
  Sem_post(&b->mutex, "insert_buffer() mutex");
  Sem_post(&b->full , "insert_buffer() full ");
}

struct item remove_buffer(struct buffer *b) {
  struct item v = { .type = ITEM_TYPE_INVALID };

  if (b == NULL || b->array == NULL) {
    return v;
  }

  Sem_wait(&b->full , "remove_buffer() full ");
  Sem_wait(&b->mutex, "remove_buffer() mutex");
  v = b->array[b->out];
  b->out = (b->out + 1) % b->size;
  b->items--;
  Sem_post(&b->mutex, "remove_buffer() mutex");
  Sem_post(&b->empty, "remove_buffer() empty");

  return v;
}

// test allocate, insert, remove, deallocate
// otherwise, not for general use
void test_buffer(int size) {
  struct item w = { .type = ITEM_TYPE_VALID, .string = "Hello, world" };
  int i, trials = 3;

  struct buffer *b = allocate_buffer(size);
  print_buffer(b);

  for (i = 0; i < trials; i++) {
      insert_buffer(b, &w);
      print_buffer(b);
  }

  for (i = 0; i < trials; i++) {
    w = remove_buffer(b);
    print_item("", &w);
    print_buffer(b);
  }

  deallocate_buffer(b);
}
