#ifndef PTHREAD_WRAPPERS_H
#define PTHREAD_WRAPPERS_H

/* pthread wrappers
 */

/*----------------------------------------------------------------------------*/

#include <pthread.h>

extern int Pthread_debug;  /* 0 = no extra printing, non-zero = extra printing */

/*----------------------------------------------------------------------------*/

/* These functions do not fail, or do not return, so no wrapper is provided.
 *   pthread_self()
 *   pthread_equal()
 *   pthread_exit()
 * These function were not needed, so no wrapper is provided.
 *   pthread_attr_init()
 *   etc.
 */

void Pthread_create(pthread_t *restrict thread,
                    const pthread_attr_t *restrict attr,
                    void *(*start_routine)(void*),
                    void *restrict arg,
                    char *msg);
void Pthread_join(pthread_t thread, void **status, char *msg);
void Pthread_cancel(pthread_t target_thread, char *msg);
void Pthread_detach(pthread_t thread, char *msg);

/*----------------------------------------------------------------------------*/

#endif
