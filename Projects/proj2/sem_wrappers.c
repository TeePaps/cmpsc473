/* semaphore wrappers
 */

/*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <semaphore.h>

#include "sem_wrappers.h"

int Sem_debug = 0;      /* 0 = no extra printing, non-zero = extra printing */

/*----------------------------------------------------------------------------*/

void Sem_init(sem_t *sem, int pshared, unsigned int value, char *msg)
{
  int ret;

  if (Sem_debug)
    {
      printf("sem_init: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = sem_init(sem, pshared, value);

  if (ret == -1)
    {
      printf("sem_init: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(errno));
      exit(1);
    }
}

void Sem_destroy(sem_t *sem, char *msg)
{
  int ret;

  if (Sem_debug)
    {
      printf("sem_destroy: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = sem_destroy(sem);

  if (ret == -1)
    {
      printf("sem_destroy: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(errno));
      exit(1);
    }
}

void Sem_wait(sem_t *sem, char *msg)
{
  int ret;

  if (Sem_debug)
    {
      printf("sem_wait: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = sem_wait(sem);

  if (ret == -1)
    {
      printf("sem_wait: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(errno));
      exit(1);
    }
}

void Sem_post(sem_t *sem, char *msg)
{
  int ret;

  if (Sem_debug)
    {
      printf("sem_post: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = sem_post(sem);

  if (ret == -1)
    {
      printf("sem_post: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(errno));
      exit(1);
    }
}

void Sem_getvalue(sem_t *sem, int *sval, char *msg)
{
  int ret;

  if (Sem_debug)
    {
      printf("sem_getvalue: thread 0x%jx, %s\n",
        (uintmax_t)pthread_self(), msg);
    }

  ret = sem_getvalue(sem, sval);

  if (ret == -1)
    {
      printf("sem_getvalue: thread 0x%jx, %s: failed: %s\n",
        (uintmax_t)pthread_self(), msg, strerror(errno));
      exit(1);
    }
}

/*----------------------------------------------------------------------------*/
