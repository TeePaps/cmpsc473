/* Thread-based Model of Printers and Users
 *
 * Important concepts
 *
 *   Define the semaphores in struct buffer,
 *   provide access to the semaphores only in the associated functions.
 *     This is an object-oriented style, it reduces complexity.
 *
 *   Initialize the semaphores before starting the threads.
 *     You don't know the order in which the threads will be scheduled.
 *
 *   Initialize struct producer, struct consumer,
 *   pass pointer to pthread_create(),
 *   save thread ID from pthread_create() for use with pthread_join(),
 *   save exit status from pthread_join().
 *
 *   Check all pointers for NULL before dereferencing.
 *
 *   Check library calls for success/failure.
 *     malloc()
 *     use the wrapper versions of pthread_*, sem_*
 *
 *   Call sem_destroy() as part of deallocate_buffer().
 *   Provide a wrapper for sem_destroy().
 *     If sem_init() is like a constructor, then sem_destroy() is like
 *     a destructor.
 *
 *   Use counters in struct producer, struct consumer.
 *     Report statistics at the end, from main().
 *     Use for debugging (stats must add up properly).
 *     Use for load-balance information.
 *
 * Less-important concepts
 *
 *   Attach serial numbers to producer, consumer.
 *     (Thread IDs tend to be harder to keep track of)
 *
 *   Attach sequence numbers to print requests.
 *     (realistic, but here it's mainly for debugging purposes)
 *
 *   Use a circular array for the buffer.
 *     (not realistic, perhaps, but here it's a lot simpler)
 *
 *   Delay printing of results until all threads are finished.
 *     (the final output is not mixed with intermediate results)
 *
 * Not implemented here, but should be
 *
 *   allocate_buffer() should return NULL instead of calling exit().
 *
 * Common mistakes
 *
 *   allocate a pointer, but do not allocate something valid it can point to
 *
 *   misunderstanding of the timing/scheduling of threads
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "pthread_wrappers.h"
#include "sem_wrappers.h"

#include <time.h>       // for nanosleep()
#include <inttypes.h>   // for uintmax_t

// hack for C99
extern int getopt(int argc, char * const argv[], const char *optstring);
extern int nanosleep(const struct timespec *rqtp, struct timespec *rmtp);
extern double drand48(void);

// global data

int verbose = 0;        /* -v option */
int delay = 0;          /* -d option */
int randomize = 0;      /* -s -S options */

/*----------------------------------------------------------------------*/
// buffer, fixed-size items

enum item_type {
  ITEM_TYPE_INVALID, ITEM_TYPE_VALID, ITEM_TYPE_TERMINATE
};

struct item {   // print request
  enum item_type type;
  int source;   // serial_number of producer
  int sequence; // sequence number from producer
  int size;     // number of bytes
};

void print_item(char *indent, struct item *v) {

  if (indent == NULL) indent = "";

  if (v == NULL) {
    printf("%sitem at NULL\n", indent);
  }
  else if (v->type == ITEM_TYPE_INVALID) {
    printf("%sitem at %p invalid\n", indent, (void *)v);
  }
  else if (v->type == ITEM_TYPE_VALID) {
    printf("%sitem at %p valid\n", indent, (void *)v);
    printf("%s  source   = %d\n", indent, v->source);
    printf("%s  sequence = %d\n", indent, v->sequence);
    printf("%s  size     = %d\n", indent, v->size);
  }
  else if (v->type == ITEM_TYPE_TERMINATE) {
    printf("%sitem at %p terminate\n", indent, (void *)v);
  }
  else {
    printf("%sitem at %p bogus\n", indent, (void *)v);
  }
}

struct buffer {
  struct item *array;
  int size, items, in, out;
  sem_t mutex, full, empty;
};

void print_buffer(struct buffer *b)
{
  int mutex_val, full_val, empty_val;

  if (b == NULL) {
    printf("buffer at NULL\n");
  }
  else {
    if (b->array == NULL) {
      printf("buffer at %p, no array\n", (void *)b);
    }
    else {
      printf("buffer at %p\n", (void *)b);
      for (int index = b->out; index != b->in; index = (index+1)%b->size) {
        print_item("  ", &b->array[index]);
      }
    }
    printf("  size = %d, items = %d, in = %d, out = %d\n",
      b->size, b->items, b->in, b->out);
    Sem_getvalue(&b->mutex, &mutex_val, "print_buffer() mutex");
    Sem_getvalue(&b->full , &full_val , "print_buffer() full ");
    Sem_getvalue(&b->empty, &empty_val, "print_buffer() empty");
    printf("  mutex val = %d, full val = %d, empty val = %d\n",
      mutex_val, full_val, empty_val);
  }
}

struct buffer * allocate_buffer(int N) {
  struct buffer *b = malloc(sizeof(struct buffer));

  if (b == NULL) {
    printf("allocate_buffer: thread 0x%jx, failed (1): %s\n",
      (uintmax_t)pthread_self(), strerror(errno));
    exit(1);  // return NULL;
  }

  b->array = malloc(N*sizeof(struct item));

  if (b->array == NULL) {
    printf("allocate_buffer: thread 0x%jx, failed (2): %s\n",
      (uintmax_t)pthread_self(), strerror(errno));
    free(b);
    exit(1);  // return NULL;
  }

  b->size = N;
  b->items = 0;
  b->in = 0;
  b->out = 0;

  Sem_init(&b->mutex, 0, 1, "allocate_buffer() mutex");
  Sem_init(&b->full , 0, 0, "allocate_buffer() full ");
  Sem_init(&b->empty, 0, N, "allocate_buffer() empty");

  return b;
}

void deallocate_buffer(struct buffer *b) {
  if (b == NULL) {
    return;
  }

  Sem_destroy(&b->mutex, "deallocate_buffer() mutex");
  Sem_destroy(&b->full , "deallocate_buffer() full ");
  Sem_destroy(&b->empty, "deallocate_buffer() empty");

  free(b->array);
  free(b);
}

void insert_buffer(struct buffer *b, struct item *v) {
  if (b == NULL || b->array == NULL || v == NULL) {
    return;
  }

  Sem_wait(&b->empty, "insert_buffer() empty");
  Sem_wait(&b->mutex, "insert_buffer() mutex");
  b->array[b->in] = *v;
  b->in = (b->in + 1) % b->size;
  b->items++;
  Sem_post(&b->mutex, "insert_buffer() mutex");
  Sem_post(&b->full , "insert_buffer() full ");
}

struct item remove_buffer(struct buffer *b) {
  struct item v = { .type = ITEM_TYPE_INVALID };

  if (b == NULL || b->array == NULL) {
    return v;
  }

  Sem_wait(&b->full , "remove_buffer() full ");
  Sem_wait(&b->mutex, "remove_buffer() mutex");
  v = b->array[b->out];
  b->out = (b->out + 1) % b->size;
  b->items--;
  Sem_post(&b->mutex, "remove_buffer() mutex");
  Sem_post(&b->empty, "remove_buffer() empty");

  return v;
}

// test allocate, insert, remove, deallocate
// otherwise, not for general use
void test_buffer(int size) {
  struct item w =
    {
      .type = ITEM_TYPE_VALID,
      .source = 17,
      .size = 18
    };
  int i, trials = 3;

  struct buffer *b = allocate_buffer(size);
  print_buffer(b);

  for (i = 0; i < trials; i++) {
      w.sequence++;
      insert_buffer(b, &w);
      print_buffer(b);
  }

  for (i = 0; i < trials; i++) {
    w = remove_buffer(b);
    print_item("", &w);
    print_buffer(b);
  }

  deallocate_buffer(b);
}

/*----------------------------------------------------------------------*/
// delay, to simulate computing or printing

void simulate_delay(time_t sec, long nsec) {
  struct timespec delay_time, remaining_time;

  delay_time.tv_sec = sec;
  delay_time.tv_nsec = nsec;
  // should enforce 0 <= tv_nsec && tv_nsec < 1,000,000,000

  if (nanosleep(&delay_time, &remaining_time) != 0) {
    // failed, let's just bail out
    printf("simulate_delay(): nanosleep() failed: %s\n", strerror(errno));
    exit(1);
  }
}

/*----------------------------------------------------------------------*/
// producer

struct producer {
  /* identifiers */
  pthread_t tid;      /* set via pthread_create() */
  int serial_number;
  /* input */
  struct buffer *print_queue;
  int max_bytes_per_request;
  int requests_to_send;
  /* output */
  int requests_sent;
  long long int bytes_sent;
  void *exit_status;  /* set via pthread_join() */
};

void init_producer(struct producer *p, int sn, struct buffer *pq, int sz, int rq) {
  if (p == NULL) {
    return;
  }

  // p->tid is set via pthread_create() in main()
  // p->exit_status is set via pthread_join() in main()
  p->tid = (pthread_t)-1;
  p->serial_number = sn;
  p->print_queue = pq;
  p->max_bytes_per_request = sz;
  p->requests_to_send = rq;
  p->requests_sent = 0;
  p->bytes_sent = 0;
  p->exit_status = (void *)-1;
}

void print_producer(struct producer *p) {
  if (p == NULL) {
    printf("producer at NULL\n"); 
  }
  else {
    printf("producer at %p\n", (void *)p);
    printf("  thread id = 0x%jx\n", (uintmax_t)p->tid);
    printf("  serial number = %d\n", p->serial_number);
    printf("  print queue = %p\n", (void *)p->print_queue);
    printf("  maximum bytes per request = %d\n", p->max_bytes_per_request);
    printf("  requests to send = %d\n", p->requests_to_send);
    printf("  requests sent = %d\n", p->requests_sent);
    printf("  total bytes sent = %lld\n", p->bytes_sent);
    printf("  exit status = %p\n", p->exit_status);
  }
}

void * producer_func(void * arg) {
  struct producer *p = (struct producer *)arg;

  if (p == NULL) {
      printf("producer_func(), thread 0x%jx, NULL arg\n", (uintmax_t)pthread_self());
      exit(1);
    }

  if (verbose) {
    printf("producer_func(), thread 0x%jx, tid 0x%jx, serial number %d, starting\n",
      (uintmax_t)pthread_self(), (uintmax_t)p->tid, p->serial_number);
  }

  if (delay) simulate_delay(0, 1000);  // startup time

  struct item v = { .type = ITEM_TYPE_VALID, .source = p->serial_number };

  for (int i = 0; i < p->requests_to_send; i++) {
    v.sequence++;

    if (randomize) {
      v.size = (int) (drand48() * p->max_bytes_per_request);
    }
    else {
      v.size = p->max_bytes_per_request;
    }
    if (v.size < 1024) {
      v.size = 1024;
    }

    if (delay) simulate_delay(0, 100 + 20*v.size);  // time to generate request

    insert_buffer(p->print_queue, &v);
    p->requests_sent++;
    p->bytes_sent += v.size;
  }

  if (delay) simulate_delay(0, 1000);  // cleanup time

  if (verbose) {
    printf("producer_func(), thread 0x%jx, tid 0x%jx, serial number %d, finished\n",
      (uintmax_t)pthread_self(), (uintmax_t)p->tid, p->serial_number);
  }

  return arg;
}

/*----------------------------------------------------------------------*/
// consumer

struct consumer {
  /* identifiers */
  pthread_t tid;      /* set via pthread_create() */
  int serial_number;
  /* input */
  struct buffer *print_queue;
  /* output */
  int requests_valid;
  int requests_invalid;
  int requests_terminate;
  long long int bytes_received;
  void *exit_status;  /* set via pthread_join() */
};

void init_consumer(struct consumer *c, int sn, struct buffer *pq) {
  if (c == NULL) {
    return;
  }

  // c->tid is set via pthread_create() in main()
  // c->exit_status is set via pthread_join() in main()
  c->tid = (pthread_t)-1;
  c->serial_number = sn;
  c->print_queue = pq;
  c->requests_valid = 0;
  c->requests_invalid = 0;
  c->requests_terminate = 0;
  c->bytes_received = 0;
  c->exit_status = (void *)-1;
}

void print_consumer(struct consumer *c) {
  if (c == NULL) {
    printf("consumer at NULL\n"); 
  }
  else {
    printf("consumer at %p\n", (void *)c);
    printf("  thread id = 0x%jx\n", (uintmax_t)c->tid);
    printf("  serial number = %d\n", c->serial_number);
    printf("  print queue = %p\n", (void *)c->print_queue);
    printf("  requests received valid = %d\n", c->requests_valid);
    printf("  requests received invalid = %d\n", c->requests_invalid);
    printf("  requests received terminate = %d\n", c->requests_terminate);
    printf("  total bytes received = %lld\n", c->bytes_received);
    printf("  exit status = %p\n", c->exit_status);
  }
}

void * consumer_func(void * arg) {
  struct consumer *c = (struct consumer *)arg;

  if (c == NULL) {
    printf("consumer_func(), thread 0x%jx, NULL arg\n", (uintmax_t)pthread_self());
    exit(1);
  }

  if (verbose) {
    printf("consumer_func(), thread 0x%jx, tid 0x%jx, serial number %d, starting\n",
      (uintmax_t)pthread_self(), (uintmax_t)c->tid, c->serial_number);
  }

  if (delay) simulate_delay(0, 1000);  // startup time

  struct item v;

  while (1) {
    v = remove_buffer(c->print_queue);

    if (v.type == ITEM_TYPE_VALID) {
      c->requests_valid++;
      c->bytes_received += v.size;
      if (delay) simulate_delay(0, 1000 + 20*v.size); // printing time
    }
    else if (v.type == ITEM_TYPE_INVALID) {
      c->requests_invalid++;
      if (delay) simulate_delay(0, 1000); // reaction time
    }
    else if (v.type == ITEM_TYPE_TERMINATE) {
      c->requests_terminate++;
      if (delay) simulate_delay(0, 1000); // reaction time
      break;
    }
    else {
      printf("bogus type in print queue\n");
      exit(1);
    }
  }

  if (delay) simulate_delay(0, 1000);  // cleanup time

  if (verbose) {
    printf("consumer_func(), thread 0x%jx, tid 0x%jx, serial number %d, finished\n",
      (uintmax_t)pthread_self(), (uintmax_t)c->tid, c->serial_number);
  }

  return arg;
}

/*----------------------------------------------------------------------*/

static void usage(char *prog, int status)
{
  if (status == EXIT_SUCCESS)
    {
      printf("usage: %s [-h] [-v] [-u num] [-p num] [-q num] [-r num] [-s num] [-S num] [-d]\n",
             prog);
      printf("  -h           print help\n");
      printf("  -v           verbose mode\n");
      printf("  -u num       the number of users (producers)\n");
      printf("  -p num       the number of printers (consumers)\n");
      printf("  -q num       the maximum number of items in the print queue\n");
      printf("  -r num       the number of print requests to generate (per user)\n");
      printf("  -s num       the constant number of bytes in a print request, basic model\n");
      printf("  -S num       the maximum number of bytes in a print request, advanced model\n");
      printf("  -d           insert a timing delay with nanosleep(), advanced model\n");
    }
  else
    {
      fprintf(stderr, "%s: Try '%s -h' for usage information.\n", prog, prog);
    }

  exit(status);
}

/*----------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
  // default values
  int print_queue_size = 16;
  int num_printers = 2;                 // consumers
  int num_users = 10;                   // producers
  int requests_per_user = 100;          // number of print requests
  int max_bytes_per_request = 10000;    // size of each print request

  /* for use with getopt(3) */
  int ch;
  extern char *optarg;
  extern int optind;
  extern int optopt;
  extern int opterr;

  /* This is only for sample output via command script. */
  printf("\n----------------------------------------------------------------------\n\n");
  for (char **p = argv; *p != NULL; p++)
  {
    printf("%s ", *p);
  }
  printf("\n\n");

  while ((ch = getopt(argc, argv, ":hvq:r:u:p:s:S:d")) != -1)
    {
      switch (ch) {
        case 'h':
          usage(argv[0], EXIT_SUCCESS);
          break;
        case 'v':
          verbose = 1;
          break;
        case 'q':
          print_queue_size = atoi(optarg);
          break;
        case 'r':
          requests_per_user = atoi(optarg);
          break;
        case 'u':
          num_users = atoi(optarg);
          break;
        case 'p':
          num_printers = atoi(optarg);
          break;
        case 's':
          max_bytes_per_request = atoi(optarg);
          randomize = 0;
          break;
        case 'S':
          max_bytes_per_request = atoi(optarg);
          randomize = 1;
          break;
        case 'd':
          delay = 1;
          break;
        case '?':
          fprintf(stderr, "%s: invalid option '%c'\n", argv[0], optopt);
          usage(argv[0], EXIT_FAILURE);
          break;
        case ':':
          fprintf(stderr, "%s: invalid option '%c' (missing argument)\n",
            argv[0], optopt);
          usage(argv[0], EXIT_FAILURE);
          break;
        default:
          usage(argv[0], EXIT_FAILURE);
          break;
      }
    }

  Pthread_debug = verbose;      /* used by Pthread_wrappers */
  Sem_debug = verbose;          /* used by Sem_wrappers */

  printf("model parameters\n");
  printf("  print queue size = %d\n", print_queue_size);
  printf("  number of printers = %d\n", num_printers);
  printf("  number of users = %d\n", num_users);
  printf("  number of print requests per user = %d\n", requests_per_user);
  printf("  maximum size of print request = %d\n", max_bytes_per_request);
  printf("  with %s request sizes\n", (randomize ? "variable" : "constant"));
  printf("  %s timing delays\n", (delay ? "with" : "without"));
  printf("\n");


  // requests generated by users
  int total_requests_sent = 0;
  long long int total_bytes_sent = 0;

  // requests received by printers
  int total_valid = 0, total_invalid = 0, total_terminate = 0;
  long long int total_bytes_received = 0;

  // initialize print_queue
  struct buffer *print_queue = allocate_buffer(print_queue_size);
  // check for NULL return value
  if (verbose) print_buffer(print_queue);

  // start printers
  struct consumer printer[num_printers];
  for (int i = 0; i < num_printers; i++)
    {
      init_consumer(&printer[i], i, print_queue);
      Pthread_create(&printer[i].tid, NULL, consumer_func, (void *)&printer[i],
        "main() printer");
      if (verbose) print_consumer(&printer[i]);
    }

  // start users
  struct producer user[num_users];
  for (int i = 0; i < num_users; i++)
    {
      init_producer(&user[i], i, print_queue, max_bytes_per_request,
        requests_per_user);
      Pthread_create(&user[i].tid, NULL, producer_func, (void *)&user[i],
        "main() user");
      if (verbose) print_producer(&user[i]);
    }

  // wait for users
  for (int i = 0; i < num_users; i++)
    {
      Pthread_join(user[i].tid, &user[i].exit_status, "main() user");
    }

  // shutdown printers
  struct item terminate = { .type = ITEM_TYPE_TERMINATE };
  for (int i = 0; i < num_printers; i++)
    {
      insert_buffer(print_queue, &terminate);
    }

  // wait for printers
  for (int i = 0; i < num_printers; i++)
    {
      Pthread_join(printer[i].tid, &printer[i].exit_status, "main() printer");
    }

  // Delay the output until all threads are finished.
  // This cleans up the output appearance.
  printf("users, requests sent, bytes sent\n");
  for (int i = 0; i < num_users; i++)
    {
      printf("  %3d  %6d  %6lld\n", user[i].serial_number,
        user[i].requests_sent, user[i].bytes_sent);
      total_requests_sent += user[i].requests_sent;
      total_bytes_sent += user[i].bytes_sent;
    }
  printf("\n");
  if (verbose)
    { for (int i = 0; i < num_users; i++)
        print_producer(&user[i]);
      printf("\n");
    }

  printf("printers, requests received (valid, invalid, terminate), bytes received\n");
  for (int i = 0; i < num_printers; i++)
    {
      printf("  %3d  %6d  %6d  %6d  %6lld\n", printer[i].serial_number,
        printer[i].requests_valid, printer[i].requests_invalid,
        printer[i].requests_terminate, printer[i].bytes_received);
      total_valid += printer[i].requests_valid;
      total_invalid += printer[i].requests_invalid;
      total_terminate += printer[i].requests_terminate;
      total_bytes_received += printer[i].bytes_received;
    }
  printf("\n");
  if (verbose)
    { for (int i = 0; i < num_printers; i++)
        print_consumer(&printer[i]);
      printf("\n");
    }

  printf("print request summary\n");
  printf("  sent, valid %d\n", total_requests_sent);
  printf("  sent, terminate %d\n", num_printers);
  printf("  received, valid, %d\n", total_valid);
  printf("  received, invalid, %d\n", total_invalid);
  printf("  received, terminate, %d\n", total_terminate);
  printf("  received, total bytes, %lld\n", total_bytes_received);

  // cleanup print_queue
  if (verbose) print_buffer(print_queue);
  deallocate_buffer(print_queue);
  print_queue = NULL;

  if (verbose) printf("all done!\n");

  return 0;
}

