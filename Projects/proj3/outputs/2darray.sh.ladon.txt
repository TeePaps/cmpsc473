
cc -std=c99 -DLOOP=0 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.427046 task-clock                #    0.090 CPUs utilized          
                 1 context-switches          #    0.002 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.232 M/sec                  
           509,791 cycles                    #    1.194 GHz                    
           233,480 stalled-cycles-frontend   #   45.80% frontend cycles idle   
           439,787 stalled-cycles-backend    #   86.27% backend  cycles idle   
           424,572 instructions              #    0.83  insns per cycle        
                                             #    1.04  stalled cycles per insn [43.74%]
     <not counted> branches                
     <not counted> branch-misses           

       0.004749348 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     <not counted> instructions            
           711,487 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           126,871 cache-references                                            
             6,782 cache-misses              #    5.346 % of all cache refs    
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.015867782 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          8.136626 task-clock                #    0.639 CPUs utilized          
                 1 context-switches          #    0.123 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               612 page-faults               #    0.075 M/sec                  
        17,821,210 cycles                    #    2.190 GHz                     [40.97%]
           121,039 stalled-cycles-frontend   #    0.68% frontend cycles idle    [51.68%]
        10,107,791 stalled-cycles-backend    #   56.72% backend  cycles idle    [55.97%]
        12,701,634 instructions              #    0.71  insns per cycle        
                                             #    0.80  stalled cycles per insn [61.54%]
         1,499,376 branches                  #  184.275 M/sec                   [63.14%]
            12,235 branch-misses             #    0.82% of all branches         [53.20%]

       0.012730424 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         8,686,102 instructions              #    0.54  insns per cycle         [52.93%]
        16,121,434 cycles                    #    0.000 GHz                     [59.29%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         2,827,658 cache-references                                             [63.57%]
             7,584 cache-misses              #    0.268 % of all cache refs     [59.10%]
               611 page-faults                                                 
         6,703,110 dTLB-loads
                                                  [46.99%]
         1,495,757 iTLB-loads
                                                  [55.92%]

       0.025164405 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=2 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

         14.009060 task-clock                #    0.761 CPUs utilized          
                 1 context-switches          #    0.071 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               611 page-faults               #    0.044 M/sec                  
        26,254,674 cycles                    #    1.874 GHz                     [50.13%]
         1,324,065 stalled-cycles-frontend   #    5.04% frontend cycles idle    [53.78%]
        23,292,203 stalled-cycles-backend    #   88.72% backend  cycles idle    [57.07%]
        12,532,444 instructions              #    0.48  insns per cycle        
                                             #    1.86  stalled cycles per insn [57.19%]
         1,365,986 branches                  #   97.507 M/sec                   [50.24%]
             3,958 branch-misses             #    0.29% of all branches         [43.12%]

       0.018397775 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        10,409,165 instructions              #    0.37  insns per cycle         [50.58%]
        28,191,034 cycles                    #    0.000 GHz                     [53.39%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         2,753,071 cache-references                                             [56.78%]
            19,480 cache-misses              #    0.708 % of all cache refs     [56.77%]
               612 page-faults                                                 
         6,782,352 dTLB-loads
                                                  [50.52%]
         1,930,292 iTLB-loads
                                                  [55.21%]

       0.030098473 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=3 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

         14.531256 task-clock                #    0.767 CPUs utilized          
                 1 context-switches          #    0.069 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               612 page-faults               #    0.042 M/sec                  
        31,849,471 cycles                    #    2.192 GHz                     [46.31%]
           131,457 stalled-cycles-frontend   #    0.41% frontend cycles idle    [50.82%]
        19,658,223 stalled-cycles-backend    #   61.72% backend  cycles idle    [53.17%]
        22,385,960 instructions              #    0.70  insns per cycle        
                                             #    0.88  stalled cycles per insn [56.53%]
         2,538,210 branches                  #  174.672 M/sec                   [58.73%]
            11,360 branch-misses             #    0.45% of all branches         [53.16%]

       0.018941547 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        18,784,132 instructions              #    0.64  insns per cycle         [51.48%]
        29,258,512 cycles                    #    0.000 GHz                     [55.09%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         4,816,698 cache-references                                             [58.22%]
             8,920 cache-misses              #    0.185 % of all cache refs     [56.17%]
               612 page-faults                                                 
        11,956,438 dTLB-loads
                                                  [49.30%]
         4,585,628 iTLB-loads
                                                  [42.41%]

       0.030593787 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=4 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

         20.121058 task-clock                #    0.805 CPUs utilized          
                 1 context-switches          #    0.050 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               612 page-faults               #    0.030 M/sec                  
        37,849,805 cycles                    #    1.881 GHz                     [52.13%]
         1,348,744 stalled-cycles-frontend   #    3.56% frontend cycles idle    [50.73%]
        31,841,580 stalled-cycles-backend    #   84.13% backend  cycles idle    [53.28%]
        22,858,731 instructions              #    0.60  insns per cycle        
                                             #    1.39  stalled cycles per insn [55.29%]
         2,533,943 branches                  #  125.935 M/sec                   [53.97%]
             8,924 branch-misses             #    0.35% of all branches         [49.00%]

       0.024987973 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        19,913,954 instructions              #    0.49  insns per cycle         [51.41%]
        40,556,898 cycles                    #    0.000 GHz                     [51.39%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         4,534,537 cache-references                                             [53.87%]
            19,942 cache-misses              #    0.440 % of all cache refs     [55.37%]
               611 page-faults                                                 
        12,297,431 dTLB-loads
                                                  [52.84%]
         4,801,109 iTLB-loads
                                                  [47.89%]

       0.036492635 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=0 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.416568 task-clock                #    0.073 CPUs utilized          
                 1 context-switches          #    0.002 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.238 M/sec                  
           863,220 cycles                    #    2.072 GHz                    
           247,771 stalled-cycles-frontend   #   28.70% frontend cycles idle   
           422,072 stalled-cycles-backend    #   48.90% backend  cycles idle   
           114,234 instructions              #    0.13  insns per cycle        
                                             #    3.69  stalled cycles per insn [ 4.32%]
     <not counted> branches                
     <not counted> branch-misses           

       0.005697029 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

           111,886 instructions              #    0.14  insns per cycle        
           779,289 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           128,602 cache-references                                            
             7,465 cache-misses              #    5.805 % of all cache refs     [67.97%]
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.016283813 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=1 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

        112.697724 task-clock                #    0.959 CPUs utilized          
                 1 context-switches          #    0.009 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.006 M/sec                  
       246,968,945 cycles                    #    2.191 GHz                     [49.43%]
         1,107,348 stalled-cycles-frontend   #    0.45% frontend cycles idle    [49.43%]
       169,060,111 stalled-cycles-backend    #   68.45% backend  cycles idle    [51.06%]
       169,118,466 instructions              #    0.68  insns per cycle        
                                             #    1.00  stalled cycles per insn [51.19%]
        17,447,489 branches                  #  154.817 M/sec                   [50.74%]
            24,084 branch-misses             #    0.14% of all branches         [50.01%]

       0.117456292 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       171,128,655 instructions              #    0.69  insns per cycle         [49.39%]
       246,741,020 cycles                    #    0.000 GHz                     [49.38%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        33,938,324 cache-references                                             [50.98%]
            25,721 cache-misses              #    0.076 % of all cache refs     [51.26%]
               642 page-faults                                                 
        93,836,371 dTLB-loads
                                                  [50.82%]
        34,536,071 iTLB-loads
                                                  [50.12%]

       0.117003551 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=2 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

       1101.527945 task-clock                #    0.995 CPUs utilized          
                 3 context-switches          #    0.003 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.583 K/sec                  
     2,417,071,930 cycles                    #    2.194 GHz                     [49.91%]
        27,514,340 stalled-cycles-frontend   #    1.14% frontend cycles idle    [50.04%]
     2,310,105,096 stalled-cycles-backend    #   95.57% backend  cycles idle    [50.08%]
       174,842,878 instructions              #    0.07  insns per cycle        
                                             #   13.21  stalled cycles per insn [50.13%]
        18,117,786 branches                  #   16.448 M/sec                   [50.09%]
            25,542 branch-misses             #    0.14% of all branches         [50.00%]

       1.107566808 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       175,062,741 instructions              #    0.07  insns per cycle         [49.91%]
     2,411,942,529 cycles                    #    0.000 GHz                     [50.06%]
                 2 context-switches                                            
                 0 cpu-migrations                                              
        35,424,189 cache-references                                             [50.10%]
            89,575 cache-misses              #    0.253 % of all cache refs     [50.12%]
               642 page-faults                                                 
       104,322,728 dTLB-loads
                                                  [50.07%]
        35,379,072 iTLB-loads
                                                  [49.98%]

       1.107034445 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=3 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

        215.691948 task-clock                #    0.977 CPUs utilized          
                 1 context-switches          #    0.005 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               641 page-faults               #    0.003 M/sec                  
       472,817,277 cycles                    #    2.192 GHz                     [49.93%]
         1,834,583 stalled-cycles-frontend   #    0.39% frontend cycles idle    [49.93%]
       338,810,695 stalled-cycles-backend    #   71.66% backend  cycles idle    [49.92%]
       335,785,226 instructions              #    0.71  insns per cycle        
                                             #    1.01  stalled cycles per insn [50.55%]
        34,129,963 branches                  #  158.235 M/sec                   [50.32%]
            31,487 branch-misses             #    0.09% of all branches         [50.09%]

       0.220719756 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       339,987,015 instructions              #    0.72  insns per cycle         [49.94%]
       472,960,077 cycles                    #    0.000 GHz                     [49.95%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        68,166,612 cache-references                                             [49.94%]
            30,492 cache-misses              #    0.045 % of all cache refs     [50.59%]
               642 page-faults                                                 
       177,486,108 dTLB-loads
                                                  [50.35%]
        68,482,964 iTLB-loads
                                                  [50.12%]

       0.233004806 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=4 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

       1206.115680 task-clock                #    0.995 CPUs utilized          
                 3 context-switches          #    0.002 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.532 K/sec                  
     2,646,438,954 cycles                    #    2.194 GHz                     [50.01%]
        28,451,665 stalled-cycles-frontend   #    1.08% frontend cycles idle    [50.01%]
     2,482,949,441 stalled-cycles-backend    #   93.82% backend  cycles idle    [50.01%]
       339,628,249 instructions              #    0.13  insns per cycle        
                                             #    7.31  stalled cycles per insn [50.11%]
        34,339,833 branches                  #   28.471 M/sec                   [50.07%]
            28,205 branch-misses             #    0.08% of all branches         [50.02%]

       1.211697574 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       340,337,102 instructions              #    0.13  insns per cycle         [50.01%]
     2,634,367,995 cycles                    #    0.000 GHz                     [50.02%]
                 2 context-switches                                            
                 0 cpu-migrations                                              
        69,815,019 cache-references                                             [50.06%]
            93,070 cache-misses              #    0.133 % of all cache refs     [50.06%]
               642 page-faults                                                 
       187,576,157 dTLB-loads
                                                  [50.00%]
        68,303,857 iTLB-loads
                                                  [50.06%]

       1.207529921 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=0 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.494923 task-clock                #    0.099 CPUs utilized          
                 1 context-switches          #    0.002 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.200 M/sec                  
           330,232 cycles                    #    0.667 GHz                    
           333,677 stalled-cycles-frontend   #  101.04% frontend cycles idle   
           504,111 stalled-cycles-backend    #  152.65% backend  cycles idle   
           414,170 instructions              #    1.25  insns per cycle        
                                             #    1.22  stalled cycles per insn [68.53%]
     <not counted> branches                
     <not counted> branch-misses           

       0.005004980 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     <not counted> instructions            
           763,488 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           124,617 cache-references                                            
             6,692 cache-misses              #    5.370 % of all cache refs    
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.016716749 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=1 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

         34.305861 task-clock                #    0.862 CPUs utilized          
                 1 context-switches          #    0.029 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.019 M/sec                  
        75,083,517 cycles                    #    2.189 GHz                     [47.53%]
         1,161,985 stalled-cycles-frontend   #    1.55% frontend cycles idle    [52.16%]
        32,095,510 stalled-cycles-backend    #   42.75% backend  cycles idle    [53.63%]
        85,184,089 instructions              #    1.13  insns per cycle        
                                             #    0.38  stalled cycles per insn [53.96%]
        17,215,120 branches                  #  501.813 M/sec                   [52.32%]
            15,734 branch-misses             #    0.09% of all branches         [49.41%]

       0.039813184 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        92,246,089 instructions              #    1.22  insns per cycle         [48.64%]
        75,905,758 cycles                    #    0.000 GHz                     [48.15%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        15,885,076 cache-references                                             [52.88%]
            22,106 cache-misses              #    0.139 % of all cache refs     [54.31%]
               642 page-faults                                                 
        26,295,489 dTLB-loads
                                                  [53.21%]
        17,966,196 iTLB-loads
                                                  [51.51%]

       0.052027965 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=2 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

       1414.865308 task-clock                #    0.994 CPUs utilized          
                 3 context-switches          #    0.002 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.454 K/sec                  
     3,104,693,395 cycles                    #    2.194 GHz                     [50.01%]
         2,474,174 stalled-cycles-frontend   #    0.08% frontend cycles idle    [49.96%]
     3,058,744,287 stalled-cycles-backend    #   98.52% backend  cycles idle    [49.96%]
        91,045,245 instructions              #    0.03  insns per cycle        
                                             #   33.60  stalled cycles per insn [50.08%]
        17,999,053 branches                  #   12.721 M/sec                   [50.10%]
            24,966 branch-misses             #    0.14% of all branches         [50.06%]

       1.423320472 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        90,714,585 instructions              #    0.03  insns per cycle         [50.01%]
     3,096,273,892 cycles                    #    0.000 GHz                     [50.04%]
                 3 context-switches                                            
                 0 cpu-migrations                                              
        18,651,815 cache-references                                             [50.07%]
           109,740 cache-misses              #    0.588 % of all cache refs     [50.09%]
               642 page-faults                                                 
        29,488,418 dTLB-loads
                                                  [50.03%]
        19,087,151 iTLB-loads
                                                  [49.95%]

       1.418533312 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=3 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

         60.172414 task-clock                #    0.908 CPUs utilized          
                 1 context-switches          #    0.017 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.011 M/sec                  
       130,283,579 cycles                    #    2.165 GHz                     [50.39%]
        19,804,882 stalled-cycles-frontend   #   15.20% frontend cycles idle    [50.14%]
        38,414,011 stalled-cycles-backend    #   29.48% backend  cycles idle    [50.15%]
       164,299,028 instructions              #    1.26  insns per cycle        
                                             #    0.23  stalled cycles per insn [50.15%]
        31,971,992 branches                  #  531.340 M/sec                   [52.15%]
            16,066 branch-misses             #    0.05% of all branches         [51.25%]

       0.066280397 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       176,989,417 instructions              #    1.32  insns per cycle         [50.59%]
       134,112,230 cycles                    #    0.000 GHz                     [51.06%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        50,024,188 cache-references                                             [51.05%]
            17,430 cache-misses              #    0.035 % of all cache refs     [49.91%]
               641 page-faults                                                 
        41,672,312 dTLB-loads
                                                  [51.12%]
        51,200,783 iTLB-loads
                                                  [50.26%]

       0.077741991 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DM=4096 -DN=4096 -DLOOP=4 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

       1440.005431 task-clock                #    0.995 CPUs utilized          
                 3 context-switches          #    0.002 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               642 page-faults               #    0.446 K/sec                  
     3,159,748,287 cycles                    #    2.194 GHz                     [50.00%]
        20,077,201 stalled-cycles-frontend   #    0.64% frontend cycles idle    [50.01%]
     3,066,459,763 stalled-cycles-backend    #   97.05% backend  cycles idle    [50.00%]
       168,398,394 instructions              #    0.05  insns per cycle        
                                             #   18.21  stalled cycles per insn [50.09%]
        34,612,635 branches                  #   24.036 M/sec                   [50.05%]
            29,159 branch-misses             #    0.08% of all branches         [50.02%]

       1.446832060 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       167,984,387 instructions              #    0.05  insns per cycle         [50.01%]
     3,201,007,238 cycles                    #    0.000 GHz                     [50.03%]
                 2 context-switches                                            
                 0 cpu-migrations                                              
        54,962,965 cache-references                                             [50.07%]
           120,321 cache-misses              #    0.219 % of all cache refs     [50.08%]
               642 page-faults                                                 
        46,812,466 dTLB-loads
                                                  [50.02%]
        50,123,795 iTLB-loads
                                                  [49.95%]

       1.467108908 seconds time elapsed


--------------------------------------------------
