#!/bin/sh

for compile in \
  "cc -std=c99 sorter.c" \
  "cc -std=c99 -DN=512 sorter.c" \
  "cc -std=c99 -DN=4096 sorter.c" \
  "cc -std=c99 -DN=2048 sorter.c" \
  "cc -std=c99 -DN=65536 sorter.c" \
  "cc -std=c99 -O sorter.c" \
  "cc -std=c99 -DN=4096 -O sorter.c"
do
  echo
  echo $compile
  echo
  $compile
  for measure in \
    "perf stat a.out" \
    "perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out"
  do
    echo $measure
    $measure
    echo
  done
  rm a.out
  echo '--------------------------------------------------'
done

