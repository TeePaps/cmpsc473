#if !defined(N)
#define N 1024
#endif

#define M N*10

#if !defined(LOOP)
#define LOOP 0
#endif

#if !defined(DATA)
#define DATA 0
#endif

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int randNums[M];

struct node {
  int data;
  struct node *next;
};
 
/* Function to return a random number */
int rrand(int m)
{
  return (int)((double)m * ( rand() / (RAND_MAX+1.0) ));
}
 
#define BYTE(X) ((unsigned char *)(X)) 

/* Function to shuffle an array of any object */
void shuffle(void *obj, size_t nmemb, size_t size)
{
  void *temp = malloc(size);
  size_t n = nmemb;
  while ( n > 1 ) {
    size_t k = rrand(n--);
    memcpy(temp, BYTE(obj) + n*size, size);
    memcpy(BYTE(obj) + n*size, BYTE(obj) + k*size, size);
    memcpy(BYTE(obj) + k*size, temp, size);
  }
  free(temp);
} 

/* Function to generate an array of unique random numbers */
void setRand() {
  for (int i = 0; i < N; i++) {
    randNums[i] = i;
  }
  shuffle(randNums, sizeof(int), N);
}

/* Function to print linked list */
void printList(struct node *head) {
  struct node *temp = head;
  while(temp != NULL) {
    printf("%d  ", temp->data);
    temp = temp->next;
  }
  printf("\n");
}

/* function to insert a new_node in a list. Note that this
  function expects a pointer to head_ref as this can modify the
  head of the input linked list (similar to push())*/
void sortedInsert(struct node** head_ref, struct node* new_node)
{
    struct node* current;
    /* Special case for the head end */
    if (*head_ref == NULL || (*head_ref)->data >= new_node->data)
    {
        new_node->next = *head_ref;
        *head_ref = new_node;
    }
    else
    {
        /* Locate the node before the point of insertion */
        current = *head_ref;
        while (current->next!=NULL &&
               current->next->data < new_node->data)
        {
            current = current->next;
        }
        new_node->next = current->next;
        current->next = new_node;
    }
}

int main() {
  // Seed random number generator with same number every run
  srand(1000);
  setRand();

  // Head of the list
  struct node* list = NULL;

  // Rest of the list
  for (int i = 1; i < N; i++) {
    struct node* new_node = malloc(sizeof(struct node));
    new_node->data = i;
    sortedInsert(&list, new_node);
  }
  //printList(list);
}


