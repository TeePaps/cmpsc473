#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef USE_MPI 
#define USE_MPI 0
#else
#define USE_MPI 1
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#if USE_MPI
#include <mpi.h>
#endif

#if !defined(N)
#define N 0
#endif

float a[N];

void quickSort(float [], int, int);
int partition(float [], int, int);
void unique_partition(float [], int, int, int [], int);
void unique_endbounds(float [], int, int [], int);

int main(int argc, char **argv) 
{
    int rank, nprocs;
    int i, len;
    //#if N == 50
    //float a[] = {4,7,57,43,59,2,46,22,59,86,42,16,73,44,67,24,62,4,22,16};
    
    #if N == 4096
    for(i = 0; i < 4096; i++)
        a[i] = (rand() % (1000000+1));
    #elif N == 20
    for(i = 0; i < 20; i++)
        a[i] = (rand() % (1000000+1));
    #elif N == 50
    for(i = 0; i < 50; i++)
        a[i] = (rand() % (1000000+1));
    #elif N == 32268
    for(i = 0; i < 32268; i++)
        a[i] = (rand() % (1000000+1));
    #elif N == 1048576
    for(i = 0; i < 1048576; i++)
        a[i] = (rand() % (1000000+1));
    #elif N == 33554432
    for(i = 0; i < 33554432; i++)
        a[i] = (rand() % (1000000+1));
    #else
    return 0;
    #endif

#if USE_MPI
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#else
    rank = 0;
    nprocs = 1;
#endif

    //find length of array a
    /*len = 0;
    for(i = 0; a[i] != NULL; i++)
        len++i;*/
    //len = sizeof(a)/sizeof(a[0]);
    len = N;
    //printf("len %d\n", len);

    int c[len];
    
    for(i = 0; i < len; i++)
    {
        c[i] = 0;
        //printf("c %d\n", c[i]);
    }
     
    /*
    printf("Before\n");
    for(i = 0; i < len; i++)
        printf("a %f\n", a[i]);
    */
    quickSort(a, 0, len-1);
    /*
    printf("Sorted\n");
    for(i = 0; i < len; i++)
        printf("a %f\n", a[i]);
    */
#ifdef _OPENMP
#pragma omp parallel num_threads(1)
#endif
{
    int tid, nthreads;
    int partition, pLo, pHi, part;

#ifdef _OPENMP
    tid = omp_get_thread_num();
    nthreads = omp_get_num_threads();
#else
    tid = 0;
    nthreads = 1;
#endif
    
    if(len % nthreads != 0) //if not evenly partitioned
    {
        partition = (len / nthreads) + 1;
        if(tid != nthreads-1)
        {
            //partition = (len / nthreads) + 1;
            pLo = partition*tid;
            pHi = partition*tid+partition;
        }
        else
        {
            part = len % nthreads;
            pLo = partition*tid;
            //pHi = partition*tid+part;
            pHi = partition*tid+(len-(nthreads-1)*partition);
        }
    }
    else
    {
        partition = len / nthreads;
        pLo = partition*tid;
        pHi = partition*tid+partition;
    }

    //pLo = partition*tid;
    //pHi = partition*tid+partition;

#pragma omp barrier 

    unique_partition(a, pLo, pHi, c, tid);
#pragma omp barrier
//#pragma omp critical

    //if(tid != nthreads-1)
      //unique_endbounds(a, pHi, c, tid);

    //printf("Rank %3d, Hello world from thread %3d of %3d\n", rank, tid, nthreads);
}

#if USE_MPI
    MPI_Finalize();
#endif
    
    int count = 0;
    //printf("Unique strings\n");
    for(i = 0; i < len; i++)
    {
        if(a[i] != '\0')
        {
            //printf("a %f %d\n", a[i], c[i]);
            count++;
        }
    }
    //printf("There are %d unique strings.\n", count);    

    return 0;
}

void quickSort(float a[], int lo, int hi)
{
    int j;
    if(lo < hi)
    {
        j = partition(a, lo, hi);
        quickSort( a, lo, j-1);
        quickSort( a, j+1, hi);
    }
}


int partition(float a[], int lo, int hi)
{
    int i, j;
    float temp;
    float pivot = a[lo];
    i = lo;
    j = hi+1;
    //printf("a[i] %s pivot %s\n",a[i], pivot);
    while(1)
    {
        do {
            i++;
        }while((a[i] <= pivot) && i <= hi);
        
        do {
            j--;
        }while(a[j] > pivot);
        if( i >= j )
            break;
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    temp = a[lo];
    a[lo] = a[j];
    a[j] = temp;

    return j;
}

void unique_partition(float a[], int lo, int hi, int count[], int tid)
{
    int i; //index of a
    int j; //index of count
    count[lo] = 1;
    j = lo;
    for(i = lo+1; i < hi; i++)
    {
        //printf("thread %d upart str %s a[i] %s strcmp %d\n", tid, str, a[i], strcmp(str, a[i]));
        if(a[j] == a[i])
        {
            count[j]++;
            a[i] = '\0';
        }
        else
        {
            count[i]++;
            j = i;
        }
    }
    //for(i = lo; i < hi; i++)
        //printf("Thread %d did a %s\n", tid, a[i]);
}

void unique_endbounds(float a[], int index, int count[], int tid)
{
    int lo = index-1, hi = index; 
    
    while(a[hi] == '\0')
    {
        hi++;
    }
    while(a[lo] == '\0')
    {
        lo--;
    }
    //printf("Thread %3d made it here index %d\n", tid, hi);
    //printf("Thread %d aindex %s aindex-1 %s strcmp %d\n", tid, a[hi], a[lo], strcmp(a[hi],a[lo]));
    if(a[hi] == a[lo])
    {
        count[lo] += count[hi];
        a[hi] = '\0';
        count[hi] = 0;
    }
} 

