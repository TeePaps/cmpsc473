#!/bin/sh
for compile in \
  "cc -std=c99 -DN=20 unique_floats.c" \
  "cc -std=c99 -DN=50 unique_floats.c" \
  "cc -std=c99 -DN=4096 unique_floats.c" \
  "cc -std=c99 -DN=32268 unique_floats.c" \
  "cc -std=c99 -DN=1048576 unique_floats.c" \
  "cc -std=c99 -DN=33554432 unique_floats.c" 
do
  echo
  echo $compile
  echo
  $compile
  for measure in \
    "perf stat ./a.out" \
    "perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads ./a.out"
  do
    echo $measure
    $measure
    echo
  done
  rm a.out
  echo '--------------------------------------------------'
done


