#include <stdio.h>

#if !defined(A) 
#define A 1024
#endif

#if !defined(B)
#define B 1024
#endif

#if !defined(C)
#define C 1024
#endif

#if !defined(D)
#define D 1024
#endif

#if !defined(E)
#define E 1024
#endif

#if !defined(LOOP)
#define LOOP 0
#endif

int data[A][B][C];

int main(void)
{
  #if LOOP == 1
    for (int i = 0; i < A; i++)
      for (int j = 0; j < B; j++)
        for (int k = 0; k < C; k++)
              data[i][j][k] = 0;

  #elif LOOP == 2
    for (int k = 0; k < C; k++)
      for (int j = 0; j < B; j++)
        for (int i = 0; i < A; i++)
          data[i][j][k] = 0;

  #elif LOOP == 3
    for (int i = 0; i < A; i++)
      for (int j = 0; j < B; j++)
        for (int k = 0; k < C; k++)
              data[i][j][k] = 0;
    for (int k = 0; k < C; k++)
      for (int j = 0; j < B; j++)
        for (int i = 0; i < A; i++)
          data[i][j][k] = 0;

  #elif LOOP == 4
    for (int k = 0; k < C; k++)
      for (int j = 0; j < B; j++)
        for (int i = 0; i < A; i++)
          data[i][j][k] = 0;
    for (int k = 0; k < C; k++)
      for (int j = 0; j < B; j++)
        for (int i = 0; i < A; i++)
          data[i][j][k] = 0;
  #else
    // nothing
  #endif

  return 0;
}

