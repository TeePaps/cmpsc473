
cc -std=c99 -DLOOP=0 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.357313 task-clock                #    0.067 CPUs utilized          
                 1 context-switches          #    0.003 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.277 M/sec                  
     <not counted> cycles                  
           199,107 stalled-cycles-frontend   #    0.00% frontend cycles idle   
           338,239 stalled-cycles-backend    #    0.00% backend  cycles idle   
           397,626 instructions              #    0.00  insns per cycle        
                                             #    0.85  stalled cycles per insn
     <not counted> branches                
     <not counted> branch-misses           

       0.005302970 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

           401,564 instructions              #    0.56  insns per cycle        
           721,899 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           131,689 cache-references                                            
     <not counted> cache-misses            
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.016333342 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

       7081.510766 task-clock                #    0.999 CPUs utilized          
                10 context-switches          #    0.001 K/sec                  
                 1 CPU-migrations            #    0.000 K/sec                  
             2,658 page-faults               #    0.375 K/sec                  
    15,517,453,126 cycles                    #    2.191 GHz                     [50.00%]
        38,672,650 stalled-cycles-frontend   #    0.25% frontend cycles idle    [50.01%]
    10,103,512,134 stalled-cycles-backend    #   65.11% backend  cycles idle    [50.01%]
    15,159,766,642 instructions              #    0.98  insns per cycle        
                                             #    0.67  stalled cycles per insn [50.01%]
     1,099,971,472 branches                  #  155.330 M/sec                   [50.00%]
         1,650,826 branch-misses             #    0.15% of all branches         [50.00%]

       7.087175828 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

    15,172,350,071 instructions              #    0.98  insns per cycle         [50.00%]
    15,452,875,557 cycles                    #    0.000 GHz                     [50.00%]
                 8 context-switches                                            
                 0 cpu-migrations                                              
     3,279,151,577 cache-references                                             [50.00%]
         1,049,130 cache-misses              #    0.032 % of all cache refs     [50.02%]
             2,658 page-faults                                                 
     7,022,242,094 dTLB-loads
                                                  [50.01%]
     3,267,143,076 iTLB-loads
                                                  [50.00%]

       7.056290019 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=2 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

     107203.548036 task-clock                #    1.000 CPUs utilized          
               127 context-switches          #    0.001 K/sec                  
                 1 CPU-migrations            #    0.000 K/sec                  
             2,658 page-faults               #    0.025 K/sec                  
   235,250,049,592 cycles                    #    2.194 GHz                     [50.00%]
       146,465,433 stalled-cycles-frontend   #    0.06% frontend cycles idle    [50.00%]
   229,407,481,944 stalled-cycles-backend    #   97.52% backend  cycles idle    [50.00%]
    15,538,073,575 instructions              #    0.07  insns per cycle        
                                             #   14.76  stalled cycles per insn [50.00%]
     1,163,795,590 branches                  #   10.856 M/sec                   [50.00%]
         2,340,558 branch-misses             #    0.20% of all branches         [50.00%]

     107.206483588 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

    15,545,469,695 instructions              #    0.06  insns per cycle         [50.00%]
   241,554,675,601 cycles                    #    0.000 GHz                     [50.00%]
               131 context-switches                                            
                 1 cpu-migrations                                              
     3,507,314,379 cache-references                                             [50.00%]
         7,987,975 cache-misses              #    0.228 % of all cache refs     [50.00%]
             2,658 page-faults                                                 
     7,527,598,723 dTLB-loads
                                                  [50.00%]
     3,507,068,768 iTLB-loads
                                                  [50.00%]

     110.078147308 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=3 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

     150969.279995 task-clock                #    1.000 CPUs utilized          
               174 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
             2,658 page-faults               #    0.018 K/sec                  
   331,292,106,342 cycles                    #    2.194 GHz                     [50.00%]
       336,790,385 stalled-cycles-frontend   #    0.10% frontend cycles idle    [50.00%]
   319,510,422,697 stalled-cycles-backend    #   96.44% backend  cycles idle    [50.00%]
    30,743,940,665 instructions              #    0.09  insns per cycle        
                                             #   10.39  stalled cycles per insn [50.00%]
     2,267,507,640 branches                  #   15.020 M/sec                   [50.00%]
         4,092,543 branch-misses             #    0.18% of all branches         [50.00%]

     150.973504469 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

    30,743,828,757 instructions              #    0.09  insns per cycle         [50.00%]
   331,499,792,449 cycles                    #    0.000 GHz                     [50.00%]
               166 context-switches                                            
                 0 cpu-migrations                                              
     5,733,024,658 cache-references                                             [50.00%]
        11,502,820 cache-misses              #    0.201 % of all cache refs     [50.00%]
             2,658 page-faults                                                 
    14,205,264,698 dTLB-loads
                                                  [50.00%]
     5,734,646,546 iTLB-loads
                                                  [50.00%]

     151.089635627 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=4 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

     219910.548593 task-clock                #    1.000 CPUs utilized          
               255 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
             2,658 page-faults               #    0.012 K/sec                  
   482,589,790,449 cycles                    #    2.194 GHz                     [50.00%]
       350,707,810 stalled-cycles-frontend   #    0.07% frontend cycles idle    [50.00%]
   470,699,768,023 stalled-cycles-backend    #   97.54% backend  cycles idle    [50.00%]
    31,006,682,475 instructions              #    0.06  insns per cycle        
                                             #   15.18  stalled cycles per insn [50.00%]
     2,312,012,757 branches                  #   10.513 M/sec                   [50.00%]
         4,563,273 branch-misses             #    0.20% of all branches         [50.00%]

     219.914641921 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

    31,097,117,932 instructions              #    0.06  insns per cycle         [50.00%]
   536,498,892,456 cycles                    #    0.000 GHz                     [50.00%]
               287 context-switches                                            
                 2 cpu-migrations                                              
     6,410,260,151 cache-references                                             [50.00%]
        17,848,399 cache-misses              #    0.278 % of all cache refs     [50.00%]
             2,658 page-faults                                                 
    15,180,940,514 dTLB-loads
                                                  [50.00%]
     6,410,985,203 iTLB-loads
                                                  [50.00%]

     244.470814058 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=0 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.352838 task-clock                #    0.067 CPUs utilized          
                 1 context-switches          #    0.003 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.281 M/sec                  
           765,198 cycles                    #    2.169 GHz                    
           203,010 stalled-cycles-frontend   #   26.53% frontend cycles idle   
           325,762 stalled-cycles-backend    #   42.57% backend  cycles idle   
     <not counted> instructions            
     <not counted> branches                
     <not counted> branch-misses           

       0.005264268 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

           392,635 instructions              #    0.56  insns per cycle        
           696,688 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           128,482 cache-references                                            
     <not counted> cache-misses            
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.017343405 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=1 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

        891.491890 task-clock                #    0.994 CPUs utilized          
                 3 context-switches          #    0.003 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.971 K/sec                  
     1,954,067,955 cycles                    #    2.192 GHz                     [49.87%]
         6,748,857 stalled-cycles-frontend   #    0.35% frontend cycles idle    [50.07%]
     1,269,637,367 stalled-cycles-backend    #   64.97% backend  cycles idle    [50.12%]
     1,895,425,481 instructions              #    0.97  insns per cycle        
                                             #    0.67  stalled cycles per insn [50.15%]
       138,583,610 branches                  #  155.451 M/sec                   [50.06%]
           340,509 branch-misses             #    0.25% of all branches         [49.96%]

       0.897208161 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     1,893,678,300 instructions              #    0.97  insns per cycle         [50.01%]
     1,948,670,798 cycles                    #    0.000 GHz                     [50.07%]
                 2 context-switches                                            
                 0 cpu-migrations                                              
       411,929,303 cache-references                                             [50.12%]
           143,170 cache-misses              #    0.035 % of all cache refs     [50.14%]
               866 page-faults                                                 
       880,867,835 dTLB-loads
                                                  [50.03%]
       412,146,710 iTLB-loads
                                                  [49.92%]

       0.896685124 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=2 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      13088.053877 task-clock                #    0.999 CPUs utilized          
                15 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.066 K/sec                  
    28,711,985,069 cycles                    #    2.194 GHz                     [50.00%]
        20,185,849 stalled-cycles-frontend   #    0.07% frontend cycles idle    [50.00%]
    27,982,778,951 stalled-cycles-backend    #   97.46% backend  cycles idle    [50.01%]
     1,945,917,409 instructions              #    0.07  insns per cycle        
                                             #   14.38  stalled cycles per insn [50.01%]
       145,958,506 branches                  #   11.152 M/sec                   [50.00%]
           431,811 branch-misses             #    0.30% of all branches         [49.99%]

      13.094708121 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     1,946,160,012 instructions              #    0.07  insns per cycle         [50.00%]
    28,987,954,234 cycles                    #    0.000 GHz                     [50.00%]
                15 context-switches                                            
                 0 cpu-migrations                                              
       430,805,626 cache-references                                             [50.00%]
           951,692 cache-misses              #    0.221 % of all cache refs     [50.01%]
               866 page-faults                                                 
       940,115,645 dTLB-loads
                                                  [50.01%]
       431,397,711 iTLB-loads
                                                  [50.00%]

      13.218768777 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=3 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      14087.393317 task-clock                #    1.000 CPUs utilized          
                15 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.061 K/sec                  
    30,909,611,979 cycles                    #    2.194 GHz                     [50.00%]
        26,499,921 stalled-cycles-frontend   #    0.09% frontend cycles idle    [50.00%]
    29,491,928,489 stalled-cycles-backend    #   95.41% backend  cycles idle    [50.00%]
     3,828,683,193 instructions              #    0.12  insns per cycle        
                                             #    7.70  stalled cycles per insn [50.01%]
       281,496,966 branches                  #   19.982 M/sec                   [50.01%]
           761,159 branch-misses             #    0.27% of all branches         [50.00%]

      14.089166061 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     3,827,321,654 instructions              #    0.13  insns per cycle         [50.00%]
    30,373,491,081 cycles                    #    0.000 GHz                     [50.00%]
                16 context-switches                                            
                 0 cpu-migrations                                              
       700,586,595 cache-references                                             [50.00%]
         1,055,446 cache-misses              #    0.151 % of all cache refs     [50.00%]
               866 page-faults                                                 
     1,744,178,113 dTLB-loads
                                                  [50.01%]
       700,462,037 iTLB-loads
                                                  [50.00%]

      13.848013815 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=4 sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      26342.494790 task-clock                #    1.000 CPUs utilized          
                30 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.033 K/sec                  
    57,800,309,265 cycles                    #    2.194 GHz                     [50.00%]
        39,464,431 stalled-cycles-frontend   #    0.07% frontend cycles idle    [50.00%]
    56,331,069,495 stalled-cycles-backend    #   97.46% backend  cycles idle    [50.00%]
     3,876,864,355 instructions              #    0.07  insns per cycle        
                                             #   14.53  stalled cycles per insn [50.00%]
       289,972,705 branches                  #   11.008 M/sec                   [50.00%]
           850,235 branch-misses             #    0.29% of all branches         [50.00%]

      26.344594048 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     3,873,054,569 instructions              #    0.07  insns per cycle         [50.00%]
    57,289,134,996 cycles                    #    0.000 GHz                     [50.00%]
                30 context-switches                                            
                 0 cpu-migrations                                              
       720,330,691 cache-references                                             [50.00%]
         1,946,109 cache-misses              #    0.270 % of all cache refs     [50.00%]
               866 page-faults                                                 
     1,804,897,896 dTLB-loads
                                                  [50.01%]
       720,590,967 iTLB-loads
                                                  [50.00%]

      26.135693550 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=0 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

          0.458267 task-clock                #    0.083 CPUs utilized          
                 1 context-switches          #    0.002 M/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
                99 page-faults               #    0.216 M/sec                  
           274,064 cycles                    #    0.598 GHz                    
           273,255 stalled-cycles-frontend   #   99.70% frontend cycles idle   
           474,779 stalled-cycles-backend    #  173.24% backend  cycles idle   
           415,165 instructions              #    1.51  insns per cycle        
                                             #    1.14  stalled cycles per insn [71.42%]
     <not counted> branches                
     <not counted> branch-misses           

       0.005490961 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

           391,790 instructions              #    0.56  insns per cycle        
           705,554 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           129,347 cache-references                                            
     <not counted> cache-misses            
                99 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.017644714 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=1 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

        271.497530 task-clock                #    0.975 CPUs utilized          
                 1 context-switches          #    0.004 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.003 M/sec                  
       592,226,203 cycles                    #    2.181 GHz                     [50.09%]
        27,801,643 stalled-cycles-frontend   #    4.69% frontend cycles idle    [50.28%]
       255,939,004 stalled-cycles-backend    #   43.22% backend  cycles idle    [50.27%]
       685,823,118 instructions              #    1.16  insns per cycle        
                                             #    0.37  stalled cycles per insn [50.10%]
       137,502,174 branches                  #  506.458 M/sec                   [50.27%]
           311,220 branch-misses             #    0.23% of all branches         [50.10%]

       0.278494626 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       683,934,545 instructions              #    1.18  insns per cycle         [50.05%]
       579,200,899 cycles                    #    0.000 GHz                     [50.13%]
                 2 context-switches                                            
                 0 cpu-migrations                                              
       138,851,868 cache-references                                             [50.14%]
            86,577 cache-misses              #    0.062 % of all cache refs     [50.12%]
               866 page-faults                                                 
       207,066,491 dTLB-loads
                                                  [50.42%]
       139,971,557 iTLB-loads
                                                  [50.23%]

       0.282502488 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=2 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      11791.345296 task-clock                #    1.000 CPUs utilized          
                13 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.073 K/sec                  
    25,867,709,736 cycles                    #    2.194 GHz                     [50.00%]
        16,566,706 stalled-cycles-frontend   #    0.06% frontend cycles idle    [50.01%]
    25,521,486,403 stalled-cycles-backend    #   98.66% backend  cycles idle    [50.01%]
       732,059,864 instructions              #    0.03  insns per cycle        
                                             #   34.86  stalled cycles per insn [50.01%]
       144,330,755 branches                  #   12.240 M/sec                   [50.00%]
           427,987 branch-misses             #    0.30% of all branches         [50.01%]

      11.793201724 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       731,465,396 instructions              #    0.03  insns per cycle         [50.00%]
    25,990,645,156 cycles                    #    0.000 GHz                     [50.01%]
                14 context-switches                                            
                 0 cpu-migrations                                              
       151,594,126 cache-references                                             [50.01%]
           877,691 cache-misses              #    0.579 % of all cache refs     [50.00%]
               866 page-faults                                                 
       228,619,151 dTLB-loads
                                                  [50.00%]
       150,671,465 iTLB-loads
                                                  [50.00%]

      11.869674765 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=3 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      11877.404834 task-clock                #    1.000 CPUs utilized          
                14 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.073 K/sec                  
    26,059,834,944 cycles                    #    2.194 GHz                     [49.99%]
        33,032,444 stalled-cycles-frontend   #    0.13% frontend cycles idle    [50.00%]
    25,418,277,210 stalled-cycles-backend    #   97.54% backend  cycles idle    [50.01%]
     1,408,669,020 instructions              #    0.05  insns per cycle        
                                             #   18.04  stalled cycles per insn [50.01%]
       279,683,991 branches                  #   23.548 M/sec                   [50.01%]
           712,364 branch-misses             #    0.25% of all branches         [50.00%]

      11.883235755 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     1,405,708,652 instructions              #    0.05  insns per cycle         [50.00%]
    26,303,885,384 cycles                    #    0.000 GHz                     [50.00%]
                13 context-switches                                            
                 0 cpu-migrations                                              
       287,326,069 cache-references                                             [50.00%]
           927,983 cache-misses              #    0.323 % of all cache refs     [50.00%]
               866 page-faults                                                 
       365,564,739 dTLB-loads
                                                  [50.00%]
       289,147,274 iTLB-loads
                                                  [50.01%]

      11.991764975 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=4 -O sec995.c

perf stat a.out

 Performance counter stats for 'a.out':

      23372.445291 task-clock                #    1.000 CPUs utilized          
                26 context-switches          #    0.001 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               866 page-faults               #    0.037 K/sec                  
    51,282,457,280 cycles                    #    2.194 GHz                     [49.99%]
        30,965,725 stalled-cycles-frontend   #    0.06% frontend cycles idle    [50.00%]
    50,628,621,841 stalled-cycles-backend    #   98.73% backend  cycles idle    [50.00%]
     1,451,626,718 instructions              #    0.03  insns per cycle        
                                             #   34.88  stalled cycles per insn [50.01%]
       287,394,013 branches                  #   12.296 M/sec                   [50.00%]
           818,403 branch-misses             #    0.28% of all branches         [50.00%]

      23.378124158 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     1,445,770,233 instructions              #    0.03  insns per cycle         [50.00%]
    51,592,411,658 cycles                    #    0.000 GHz                     [50.00%]
                26 context-switches                                            
                 0 cpu-migrations                                              
       299,438,790 cache-references                                             [50.00%]
         1,690,404 cache-misses              #    0.565 % of all cache refs     [50.00%]
               866 page-faults                                                 
       385,146,016 dTLB-loads
                                                  [50.00%]
       298,456,504 iTLB-loads
                                                  [50.00%]

      23.517941585 seconds time elapsed


--------------------------------------------------
