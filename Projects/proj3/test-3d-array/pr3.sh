#!/bin/sh

for compile in \
  "cc -std=c99 -DLOOP=0 sec995.c" \
  "cc -std=c99 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DLOOP=3 sec995.c" \
  "cc -std=c99 -DLOOP=4 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=0 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=3 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=4 sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=0 -O sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=1 -O sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=2 -O sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=3 -O sec995.c" \
  "cc -std=c99 -DA=512 -DB=512 -DC=512 -DD=512 -DE=512 -DLOOP=4 -O sec995.c" 
do
  echo
  echo $compile
  echo
  $compile
  for measure in \
    "perf stat a.out" \
    "perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out"
  do
    echo $measure
    $measure
    echo
  done
  rm a.out
  echo '--------------------------------------------------'
done

