#if !defined(M)
#define M 1024
#endif

#if !defined(N)
#define N 1024
#endif

#if !defined(LOOP)
#define LOOP 0
#endif

int data[M][N];

int main(void)
{
  #if LOOP == 1
    for (int i = 0; i < M; i++)
      for (int j = 0; j <  N; j++)
        data[i][j] = 0;
  #elif LOOP == 2
    for (int j = 0; j <  N; j++)
      for (int i = 0; i < M; i++)
        data[i][j] = 0;
  #elif LOOP == 3
    for (int i = 0; i < M; i++)
      for (int j = 0; j <  N; j++)
        data[i][j] = 0;
    for (int i = 0; i < M; i++)
      for (int j = 0; j <  N; j++)
        data[i][j] = 0;
  #elif LOOP == 4
    for (int j = 0; j <  N; j++)
      for (int i = 0; i < M; i++)
        data[i][j] = 0;
    for (int i = 0; i < M; i++)
      for (int j = 0; j <  N; j++)
        data[i][j] = 0;
  #else
    // nothing
  #endif

  return 0;
}
