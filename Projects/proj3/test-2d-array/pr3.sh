#!/bin/sh

for compile in \
  "cc -std=c99 -DLOOP=0 sec995.c" \
  "cc -std=c99 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DLOOP=3 sec995.c" \
  "cc -std=c99 -DLOOP=4 sec995.c" \
  "cc -std=c99 -DN=4096 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DM=4096 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DN=4096 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=4096 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=512 -DN=4096 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=512 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=512 -DN=4096 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=512 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=0 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=3 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=4 sec995.c" \
  "cc -std=c99 -DM=9182 -DN=9182 -DLOOP=1 sec995.c" \
  "cc -std=c99 -DM=9182 -DN=9182 -DLOOP=2 sec995.c" \
  "cc -std=c99 -DM=9182 -DN=9182 -DLOOP=3 sec995.c" \
  "cc -std=c99 -DM=9182 -DN=9182 -DLOOP=4 sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=0 -O sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=1 -O sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=2 -O sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=3 -O sec995.c" \
  "cc -std=c99 -DM=4096 -DN=4096 -DLOOP=4 -O sec995.c"
do
  echo
  echo $compile
  echo
  $compile
  for measure in \
    "perf stat a.out" \
    "perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out"
  do
    echo $measure
    $measure
    echo
  done
  rm a.out
  echo '--------------------------------------------------'
done

