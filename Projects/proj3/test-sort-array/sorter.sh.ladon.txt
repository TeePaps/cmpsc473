
cc -std=c99 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.837807 task-clock                #    0.526 CPUs utilized          
                 1 context-switches          #    0.207 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.024 M/sec                  
        10,583,786 cycles                    #    2.188 GHz                     [62.41%]
           223,173 stalled-cycles-frontend   #    2.11% frontend cycles idle    [41.88%]
         6,851,750 stalled-cycles-backend    #   64.74% backend  cycles idle    [38.16%]
         7,556,230 instructions              #    0.71  insns per cycle        
                                             #    0.91  stalled cycles per insn [37.77%]
           671,382 branches                  #  138.778 M/sec                  
             1,037 branch-misses             #    0.15% of all branches         [78.51%]

       0.009204649 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         7,987,201 instructions              #    0.77  insns per cycle         [38.78%]
        10,402,156 cycles                    #    0.000 GHz                     [37.02%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
           537,033 cache-references                                            
             6,307 cache-misses              #    1.174 % of all cache refs    
               117 page-faults                                                 
         4,746,248 dTLB-loads
                                                  [76.57%]
         1,184,021 iTLB-loads
                                                  [59.80%]

       0.020865378 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DDATA=1 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.793071 task-clock                #    0.530 CPUs utilized          
                 1 context-switches          #    0.209 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               117 page-faults               #    0.024 M/sec                  
        10,511,607 cycles                    #    2.193 GHz                     [53.97%]
           423,328 stalled-cycles-frontend   #    4.03% frontend cycles idle    [37.55%]
         6,873,220 stalled-cycles-backend    #   65.39% backend  cycles idle    [37.48%]
         3,520,219 instructions              #    0.33  insns per cycle        
                                             #    1.95  stalled cycles per insn
           780,928 branches                  #  162.929 M/sec                   [93.39%]
               936 branch-misses             #    0.12% of all branches         [71.28%]

       0.009051911 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         8,016,903 instructions              #    0.77  insns per cycle         [38.30%]
        10,458,397 cycles                    #    0.000 GHz                     [37.34%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
           539,314 cache-references                                            
             6,409 cache-misses              #    1.188 % of all cache refs    
               118 page-faults                                                 
         4,780,048 dTLB-loads
                                                  [75.85%]
         1,190,057 iTLB-loads
                                                  [59.20%]

       0.021123591 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DDATA=2 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.806591 task-clock                #    0.533 CPUs utilized          
                 1 context-switches          #    0.208 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.025 M/sec                  
        10,507,416 cycles                    #    2.186 GHz                     [37.72%]
           600,365 stalled-cycles-frontend   #    5.71% frontend cycles idle    [37.66%]
         3,118,865 stalled-cycles-backend    #   29.68% backend  cycles idle   
         5,103,832 instructions              #    0.49  insns per cycle        
                                             #    0.61  stalled cycles per insn [96.53%]
           994,255 branches                  #  206.852 M/sec                   [73.08%]
             3,545 branch-misses             #    0.36% of all branches         [56.27%]

       0.009017985 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         7,533,042 instructions              #    0.72  insns per cycle         [37.40%]
        10,439,687 cycles                    #    0.000 GHz                     [37.32%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
           644,869 cache-references                                            
               772 cache-misses              #    0.120 % of all cache refs     [88.62%]
               118 page-faults                                                 
         5,295,095 dTLB-loads
                                                  [68.48%]
         1,193,793 iTLB-loads
                                                  [50.25%]

       0.021348993 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.767776 task-clock                #    0.453 CPUs utilized          
                 1 context-switches          #    0.210 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.025 M/sec                  
        10,418,815 cycles                    #    2.185 GHz                     [37.22%]
           601,646 stalled-cycles-frontend   #    5.77% frontend cycles idle    [37.13%]
         3,653,261 stalled-cycles-backend    #   35.06% backend  cycles idle   
         5,847,957 instructions              #    0.56  insns per cycle        
                                             #    0.62  stalled cycles per insn [86.82%]
         1,080,641 branches                  #  226.655 M/sec                   [67.41%]
             3,942 branch-misses             #    0.36% of all branches         [48.65%]

       0.010526997 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         7,544,130 instructions              #    0.72  insns per cycle         [37.54%]
        10,463,728 cycles                    #    0.000 GHz                     [37.43%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
           643,777 cache-references                                            
               631 cache-misses              #    0.098 % of all cache refs     [88.69%]
               118 page-faults                                                 
         5,305,555 dTLB-loads
                                                  [68.54%]
         1,199,987 iTLB-loads
                                                  [50.42%]

       0.022101572 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -DDATA=1 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.768856 task-clock                #    0.511 CPUs utilized          
                 1 context-switches          #    0.210 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.025 M/sec                  
        10,422,467 cycles                    #    2.186 GHz                     [37.22%]
           595,667 stalled-cycles-frontend   #    5.72% frontend cycles idle    [37.14%]
         3,809,856 stalled-cycles-backend    #   36.55% backend  cycles idle   
         6,012,222 instructions              #    0.58  insns per cycle        
                                             #    0.63  stalled cycles per insn [84.45%]
         1,104,156 branches                  #  231.535 M/sec                   [65.96%]
             4,024 branch-misses             #    0.36% of all branches         [46.62%]

       0.009333505 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         7,614,926 instructions              #    0.73  insns per cycle         [37.65%]
        10,479,787 cycles                    #    0.000 GHz                     [37.57%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
           614,780 cache-references                                            
             1,734 cache-misses              #    0.282 % of all cache refs     [91.62%]
               118 page-faults                                                 
         5,181,584 dTLB-loads
                                                  [70.25%]
         1,200,646 iTLB-loads
                                                  [52.76%]

       0.021630569 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -DDATA=2 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          4.763452 task-clock                #    0.513 CPUs utilized          
                 1 context-switches          #    0.210 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.025 M/sec                  
        10,407,564 cycles                    #    2.185 GHz                     [37.16%]
           589,433 stalled-cycles-frontend   #    5.66% frontend cycles idle    [37.07%]
         3,983,682 stalled-cycles-backend    #   38.28% backend  cycles idle   
         6,198,967 instructions              #    0.60  insns per cycle        
                                             #    0.64  stalled cycles per insn [81.91%]
         1,131,454 branches                  #  237.528 M/sec                   [64.42%]
             4,134 branch-misses             #    0.37% of all branches         [44.26%]

       0.009280205 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

         8,016,696 instructions              #    0.77  insns per cycle         [40.55%]
        10,473,650 cycles                    #    0.000 GHz                     [37.51%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         1,329,371 cache-references                                             [39.03%]
             6,351 cache-misses              #    0.478 % of all cache refs    
               118 page-faults                                                 
         4,652,953 dTLB-loads
                                                  [77.81%]
         1,190,339 iTLB-loads
                                                  [61.21%]

       0.020711247 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DN=4096 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.685725 task-clock                #    0.941 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       152,868,665 cycles                    #    2.194 GHz                     [48.35%]
           435,611 stalled-cycles-frontend   #    0.28% frontend cycles idle    [51.08%]
       107,938,784 stalled-cycles-backend    #   70.61% backend  cycles idle    [51.79%]
       118,148,605 instructions              #    0.77  insns per cycle        
                                             #    0.91  stalled cycles per insn [51.83%]
        16,953,804 branches                  #  243.289 M/sec                   [50.57%]
             7,917 branch-misses             #    0.05% of all branches         [49.13%]

       0.074072104 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,187,775 instructions              #    0.80  insns per cycle         [48.48%]
       148,516,413 cycles                    #    0.000 GHz                     [51.26%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,876,737 cache-references                                             [51.97%]
             6,392 cache-misses              #    0.038 % of all cache refs     [51.64%]
               134 page-faults                                                 
        84,595,825 dTLB-loads
                                                  [50.33%]
        17,044,388 iTLB-loads
                                                  [48.90%]

       0.085631289 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DN=4096 -DDATA=1 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.791916 task-clock                #    0.929 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       153,098,004 cycles                    #    2.194 GHz                     [48.43%]
           456,951 stalled-cycles-frontend   #    0.30% frontend cycles idle    [50.96%]
       107,730,262 stalled-cycles-backend    #   70.37% backend  cycles idle    [51.68%]
       117,658,483 instructions              #    0.77  insns per cycle        
                                             #    0.92  stalled cycles per insn [51.95%]
        16,948,071 branches                  #  242.837 M/sec                   [50.87%]
             8,043 branch-misses             #    0.05% of all branches         [49.44%]

       0.075116577 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,405,264 instructions              #    0.81  insns per cycle         [48.55%]
       146,821,443 cycles                    #    0.000 GHz                     [50.69%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,685,954 cache-references                                             [51.42%]
            17,884 cache-misses              #    0.107 % of all cache refs     [52.12%]
               134 page-faults                                                 
        84,522,528 dTLB-loads
                                                  [51.41%]
        17,059,892 iTLB-loads
                                                  [49.98%]

       0.075201225 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DN=4096 -DDATA=2 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.740519 task-clock                #    0.940 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       152,996,113 cycles                    #    2.194 GHz                     [48.39%]
           426,107 stalled-cycles-frontend   #    0.28% frontend cycles idle    [51.11%]
       108,038,830 stalled-cycles-backend    #   70.62% backend  cycles idle    [51.81%]
       118,078,620 instructions              #    0.77  insns per cycle        
                                             #    0.91  stalled cycles per insn [51.80%]
        16,948,297 branches                  #  243.019 M/sec                   [50.55%]
             7,730 branch-misses             #    0.05% of all branches         [49.12%]

       0.074190171 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,374,755 instructions              #    0.80  insns per cycle         [48.52%]
       147,218,023 cycles                    #    0.000 GHz                     [50.83%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,716,582 cache-references                                             [51.55%]
            14,546 cache-misses              #    0.087 % of all cache refs     [52.08%]
               134 page-faults                                                 
        84,611,688 dTLB-loads
                                                  [51.21%]
        17,048,726 iTLB-loads
                                                  [49.79%]

       0.086394458 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -DN=4096 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.814497 task-clock                #    0.937 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       153,174,914 cycles                    #    2.194 GHz                     [49.59%]
           404,038 stalled-cycles-frontend   #    0.26% frontend cycles idle    [48.44%]
       106,036,520 stalled-cycles-backend    #   69.23% backend  cycles idle    [50.90%]
       115,468,808 instructions              #    0.75  insns per cycle        
                                             #    0.92  stalled cycles per insn [51.61%]
        16,802,451 branches                  #  240.673 M/sec                   [52.02%]
             7,910 branch-misses             #    0.05% of all branches         [51.01%]

       0.074500971 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,174,412 instructions              #    0.80  insns per cycle         [48.45%]
       147,858,948 cycles                    #    0.000 GHz                     [51.04%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,813,753 cache-references                                             [51.76%]
             6,016 cache-misses              #    0.036 % of all cache refs     [51.86%]
               133 page-faults                                                 
        84,591,940 dTLB-loads
                                                  [50.73%]
        17,051,672 iTLB-loads
                                                  [49.30%]

       0.086328024 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -DN=4096 -DDATA=1 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.743106 task-clock                #    0.926 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       152,995,054 cycles                    #    2.194 GHz                     [48.39%]
           434,138 stalled-cycles-frontend   #    0.28% frontend cycles idle    [50.98%]
       107,769,072 stalled-cycles-backend    #   70.44% backend  cycles idle    [51.70%]
       117,895,034 instructions              #    0.77  insns per cycle        
                                             #    0.91  stalled cycles per insn [51.93%]
        16,943,664 branches                  #  242.944 M/sec                   [50.80%]
             7,662 branch-misses             #    0.05% of all branches         [49.36%]

       0.075284198 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,107,526 instructions              #    0.80  insns per cycle         [48.42%]
       148,519,263 cycles                    #    0.000 GHz                     [51.26%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,898,012 cache-references                                             [51.97%]
             6,148 cache-misses              #    0.036 % of all cache refs     [51.64%]
               134 page-faults                                                 
        84,604,441 dTLB-loads
                                                  [50.27%]
        17,059,349 iTLB-loads
                                                  [48.83%]

       0.085203912 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -DN=4096 -DDATA=2 sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         69.781538 task-clock                #    0.925 CPUs utilized          
                 1 context-switches          #    0.014 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.002 M/sec                  
       153,068,531 cycles                    #    2.194 GHz                     [48.42%]
           432,003 stalled-cycles-frontend   #    0.28% frontend cycles idle    [51.28%]
       108,447,918 stalled-cycles-backend    #   70.85% backend  cycles idle    [52.00%]
       118,557,932 instructions              #    0.77  insns per cycle        
                                             #    0.91  stalled cycles per insn [51.61%]
        16,940,202 branches                  #  242.761 M/sec                   [50.22%]
             7,921 branch-misses             #    0.05% of all branches         [48.78%]

       0.075418358 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

       118,266,589 instructions              #    0.80  insns per cycle         [48.39%]
       147,399,566 cycles                    #    0.000 GHz                     [50.90%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
        16,778,967 cache-references                                             [51.61%]
             9,724 cache-misses              #    0.058 % of all cache refs     [52.01%]
               134 page-faults                                                 
        84,603,182 dTLB-loads
                                                  [50.96%]
        17,047,000 iTLB-loads
                                                  [49.53%]

       0.085692289 seconds time elapsed


--------------------------------------------------

cc -std=c99 -O sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          1.399930 task-clock                #    0.242 CPUs utilized          
                 1 context-switches          #    0.714 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.084 M/sec                  
     <not counted> cycles                  
           171,554 stalled-cycles-frontend   #    0.00% frontend cycles idle   
         1,300,603 stalled-cycles-backend    #    0.00% backend  cycles idle   
         4,188,248 instructions              #    0.00  insns per cycle        
                                             #    0.31  stalled cycles per insn
         1,955,259 branches                  # 1396.683 M/sec                   [30.36%]
     <not counted> branch-misses           

       0.005787672 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

           933,050 instructions              #    0.31  insns per cycle        
         3,049,994 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           713,882 cache-references                                            
             2,841 cache-misses              #    0.398 % of all cache refs     [65.07%]
               118 page-faults                                                 
     <not counted> dTLB-loads
             
     <not counted> iTLB-loads
             

       0.017850274 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DLOOP=1 -O sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

          1.402922 task-clock                #    0.243 CPUs utilized          
                 1 context-switches          #    0.713 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               118 page-faults               #    0.084 M/sec                  
         1,601,968 cycles                    #    1.142 GHz                    
           222,903 stalled-cycles-frontend   #   13.91% frontend cycles idle   
         1,303,477 stalled-cycles-backend    #   81.37% backend  cycles idle   
         4,968,770 instructions              #    3.10  insns per cycle        
                                             #    0.26  stalled cycles per insn [47.65%]
     <not counted> branches                
     <not counted> branch-misses           

       0.005781460 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

     <not counted> instructions            
         2,019,074 cycles                    #    0.000 GHz                    
                 1 context-switches                                            
                 0 cpu-migrations                                              
           711,818 cache-references                                            
             7,689 cache-misses              #    1.080 % of all cache refs    
               118 page-faults                                                 
         1,388,882 dTLB-loads
                                                  [33.38%]
     <not counted> iTLB-loads
             

       0.017612287 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DN=4096 -DDATA=1 -O sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         15.918891 task-clock                #    0.785 CPUs utilized          
                 1 context-switches          #    0.063 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.008 M/sec                  
        34,886,628 cycles                    #    2.192 GHz                     [43.51%]
           373,402 stalled-cycles-frontend   #    1.07% frontend cycles idle    [55.69%]
        13,374,663 stalled-cycles-backend    #   38.34% backend  cycles idle    [58.73%]
        59,030,094 instructions              #    1.69  insns per cycle        
                                             #    0.23  stalled cycles per insn [57.89%]
        25,762,814 branches                  # 1618.380 M/sec                   [52.65%]
             7,125 branch-misses             #    0.03% of all branches         [46.37%]

       0.020268376 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        60,027,139 instructions              #    1.72  insns per cycle         [44.14%]
        34,884,636 cycles                    #    0.000 GHz                     [43.49%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         7,522,970 cache-references                                             [56.82%]
            12,324 cache-misses              #    0.164 % of all cache refs     [59.71%]
               134 page-faults                                                 
        17,248,184 dTLB-loads
                                                  [56.60%]
         8,694,507 iTLB-loads
                                                  [50.41%]

       0.021149216 seconds time elapsed


--------------------------------------------------

cc -std=c99 -DN=4096 -DDATA=2 -O sorter.c

perf stat a.out

 Performance counter stats for 'a.out':

         15.918820 task-clock                #    0.780 CPUs utilized          
                 1 context-switches          #    0.063 K/sec                  
                 0 CPU-migrations            #    0.000 K/sec                  
               134 page-faults               #    0.008 M/sec                  
        34,891,263 cycles                    #    2.192 GHz                     [43.49%]
           382,919 stalled-cycles-frontend   #    1.10% frontend cycles idle    [54.31%]
        13,004,495 stalled-cycles-backend    #   37.27% backend  cycles idle    [57.52%]
        56,426,967 instructions              #    1.62  insns per cycle        
                                             #    0.23  stalled cycles per insn [59.47%]
        25,757,428 branches                  # 1618.049 M/sec                   [55.21%]
             6,992 branch-misses             #    0.03% of all branches         [48.95%]

       0.020406076 seconds time elapsed


perf stat -e instructions,cycles,context-switches,cpu-migrations,cache-references,cache-misses,page-faults,dTLB-loads,iTLB-loads a.out

 Performance counter stats for 'a.out':

        59,503,743 instructions              #    2.07  insns per cycle         [43.59%]
        28,702,411 cycles                    #    0.000 GHz                     [54.20%]
                 1 context-switches                                            
                 0 cpu-migrations                                              
         7,622,655 cache-references                                             [57.44%]
             9,670 cache-misses              #    0.127 % of all cache refs     [59.58%]
               133 page-faults                                                 
        17,263,898 dTLB-loads
                                                  [55.48%]
         8,699,729 iTLB-loads
                                                  [49.22%]

       0.031731053 seconds time elapsed


--------------------------------------------------
