#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]) {

  char* ptr;
  char* match = "oo";
  if (argc > 1) {
    match = argv[1];
  }

  char* str = "Food! food! I want food!";
  // Duplicate the string to avoid modifying the original
  char* nstr = strdup(str);
  printf("nstr [%s]\n", nstr);

  ///////////////////////////////////
  // USING strtok
  // ////////////////////////////////
  
  // First time supply string to parse
  ptr = strtok( nstr, match);
  // Count will always be one higher than the count of the delimiters,
  //    since it is counting the tokens. Therefore, start count = -1
  int count = -1;
  while (ptr != NULL) {
    printf("nstr [%s]\n", nstr);
    printf( "Next token [%s]\n", ptr);
    ptr = strtok( NULL, match);
    count++;
  }
  printf("Count [%d]\n", count);

  /////////////////////////////////////
  // USING strstr - more intuitive
  /////////////////////////////////////
  char* p = strdup(str);
  count = 0;
  while((p = strstr(p, match)) != NULL) {
    p += strlen(match);
    count++;
  }

  printf("Count [%d]\n", count);

  return( 0 );
}

