/* CMPSC 473, Project 1, starter kit
 *
 * Sample program for a pipe
 *
 * See http://www.cse.psu.edu/~dheller/cmpsc311/Lectures/Interprocess-Communication.html
 * and http://www.cse.psu.edu/~dheller/cmpsc311/Lectures/Files-Directories.html
 * for some more information, examples, and references to the CMPSC 311 textbooks.
 */

//  Description    : This is the P1
//
//   Author        : Steve Cartwright
//   Last Modified : Sat Feb  8 22:27:17 EST 2014

//--------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// This makes Solaris and Linux happy about waitpid(); it is not required on Mac OS X.
#include <sys/wait.h>

//--------------------------------------------------------------------------------
#define BUFFER_SIZE 4096
#define args "i:o:m:"

void err_sys(char *msg);				// print message and quit
char * in, * out, ** arr;
int argcount = 0, bytes, count = 0;

typedef struct Catch{
 int match, line;
 char * what, buf[BUFFER_SIZE];

} CAT;

struct Catch * catch;

	// In a pipe, the parent is upstream, and the child is downstream, connected by
	//   a pair of open file descriptors.
	// fd = file descriptor, opened by pipe()
	// treat fd as if it had come from open()


void p1(int fd);
void p2(int fd1, int fd2);
void p3(int fd);
void removeIt(char *str, char garbage, int flag);


//--------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
  int ch, i = 0;
  int fd1[2], fd2[2];			// pipe endpoints
  char **Ar;				// Arg array

  pid_t child_pid1;			// Children ids
  pid_t child_pid2;

	while ((ch = getopt(argc, argv, args)) != -1) {		// Check what args we have

		switch (ch) {
		case 'i': in = optarg;
			  //printf("%s", in);
			break;

		case 'o': out = optarg;
			   //printf("%s", out);
			break;

		case 'm': argcount++;

		/*default:  // Default (unknown)
			in = out = "/dev/null";
			return( -1 );*/
		}
	}
	Ar = arr = malloc( sizeof(char * ) * argcount);		// Create space for arg array
	optind = 1;

	while ( (ch = getopt(argc, argv, args)) != -1) {	// Count the -m options
		if( ch == 'm' ){
	          arr[i] = optarg;
   		  i++;
		}
	}

  if (pipe(fd1) < 0)
    {
      err_sys("pipe error");
    }

  if ((child_pid1 = fork()) < 0)
    {
      err_sys("fork error 1");
    }
  else if (child_pid1 > 0)
    {
	close(fd1[0]);		// Unused read
	p1(fd1[1]);
	 if (waitpid(child_pid1, NULL, 0) < 0)
		{ err_sys("waitpid error 1"); }

  	//printf(" Parent 1: PID, PPID: %d %d\n", getpid(), getppid());

  }
  else
    {	 	  close(fd1[1]);	// Kill write
		  if (pipe(fd2) < 0)
		  {    err_sys("pipe error");}
   		  if ((child_pid2 = fork()) < 0)
		  {    err_sys("fork error 2");}
		  else if(child_pid2 > 0)	// Teenager
		    {
			close(fd2[0]);		// Close pipe 2 read
			p2(fd1[0], fd2[1]);
		    if (waitpid(child_pid2, NULL, 0) < 0)
		       { err_sys("waitpid error 2"); }
	//printf(" Middle: PID, PPID: %d %d\n", getpid(), getppid());
		    }
		  else				// Child
		    {
			close(fd1[0]);		// Close pipe 1 read, pipe 2 write
		   	close(fd2[1]);
			p3(fd2[0]);
	//printf(" Child: PID, PPID: %d %d\n", getpid(), getppid());
		    }

    }

     free(Ar);	 	// Kill the arg array

  return 0;
}


void err_sys(char *msg)
{
  printf("error: PID %d, %s\n", getpid(), msg);
  exit(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p1
// Description  : first process function
//
// Inputs       : file descriptor
// Outputs      : None

void p1(int fd)
{ FILE *fq = fopen(in, "r");
  if (fq == NULL)
  { perror("fopen(r) error p1"); }

  FILE *fp = fdopen(fd, "w");
  if (fp == NULL)
  { perror("fdopen(w) error p1");}


  char buffer[BUFFER_SIZE];			// off the stack, always allocated
  int ret = setvbuf(fp, buffer, _IOLBF, BUFFER_SIZE);	// set fp to line-buffering

    if (ret != 0)
    { err_sys("setvbuf error (parent)"); }


	 while(fgets(buffer, BUFFER_SIZE, fq) != NULL){
	  bytes += strlen(buffer);
		  if(fprintf(fp, "%s", buffer) == 0) printf("ERROR no chars printed");
printf("p1 \n%s", buffer);
	  		count++;

	 }  printf("P1: file %s, bytes %d\n", in, bytes);

fclose(fq);
fclose(fp);
close(fd);

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p2
// Description  : second process function
//
// Inputs       : file descriptors
// Outputs      : None

void p2(int fd1, int fd2)
{
  char* f1, * f2;
  int l = 0, i = 0, k = 0;


  FILE *fq = fdopen(fd1, "r");
  if (fq == NULL)
  { perror("fdopen(r) error p2");}

  FILE *fp = fdopen(fd2, "w");
  if (fp == NULL)
    { perror("fdopen(w) error p2");}

  char buffer2[BUFFER_SIZE];
  char *p = NULL;
  int retr = setvbuf(fq, buffer2, _IOLBF, BUFFER_SIZE);
  int retw = setvbuf(fp, buffer2, _IOLBF, BUFFER_SIZE);
   if ((retr != 0) && (retw != 0)) {
    err_sys("setvbuf error (p2)");
  }


if((catch = calloc(argcount, sizeof(struct Catch))) == NULL)
	{ err_sys("catch error (p2)");}

	while((p = fgets(buffer2, BUFFER_SIZE, fq)) != NULL) {
printf("p2 \n%s", buffer2);
		i = 0;
		k = 0;
		while(i < argcount){
		f2 = f1 = strdup(buffer2);

			while( ((f1 = strstr(f1, arr[i])) != NULL) && strlen(arr[i]) > 0) {
				catch[i].match++;
				catch[i].what = arr[i];
				memcpy(catch[i].buf, buffer2, BUFFER_SIZE-1);
				k = 1;

				f1 += strlen(arr[i]);


			} if(k){
			     catch[i].line++;
			     k = 0;
			  }
			  if(catch[i].match > 0){
		 	     fprintf(fp, "%s", catch[i].buf);
			     //printf("%s", catch[i].buf);
			  }
			     i++;
		  	  free(f2);
	       }
	}
	while(l < argcount){
	printf("P2: string %s, lines %d, matches %d\n", catch[l].what, catch[l].line, catch[l].match);
	l++;
	}

fclose(fq);
fclose(fp);
close(fd1);
close(fd2);

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p3
// Description  : third process function
//
// Inputs       : file descriptor
// Outputs      : None


void p3(int fd){

 FILE *fq = fopen(out, "w");			// Open both files
 if (fq == NULL)
      { perror("fopen(w) error p3");}


 FILE *fp = fdopen(fd, "r");
  if (fp == NULL)
    {  perror("fdopen(r) error p3");}

char buffer3[BUFFER_SIZE];
char *p = NULL;
bytes = 0;					// Global bytes counter
  int ret = setvbuf(fp, buffer3, _IOLBF, BUFFER_SIZE);
   if (ret != 0)
    { err_sys("setvbuf error (child)"); }

	while(fgets(buffer3, BUFFER_SIZE, fp) != NULL){	// Count whats in the pipe and output
		bytes += strlen(buffer3);
printf("p3 \n%s", buffer3);
		  fprintf(fq, "%s", buffer3);

	}	printf("P3: file %s, bytes %d\n", out, bytes);

	fclose(fq);				// Close all files and descriptors
	fclose(fp);
	close(fd);
	  removeIt(p ,' ', 0);			// Kill the Catch
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : removeIt
// Description  : either remove a char or kill a catch struct
//
// Inputs       :
// Outputs      : None


void removeIt(char *str, char garbage, int flag) {
	if(flag > 1){
	    char *src, *dst;
	    for (src = dst = str; *src != '\0'; src++) {
		*dst = *src;
		if (*dst != garbage) dst++;
	    }
	    *dst = '\0';
	}
	else{ int k = 0;
		while(k < argcount){
		catch[k].what = NULL;
		catch[k].match = 0;
		catch[k].line = 0;
		k++;
		}
		free(catch);

	     }

}
//--------------------------------------------------------------------------------


