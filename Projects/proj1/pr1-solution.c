/* CMPSC 473, Project 1, solution, version 5
 */

/* changes, pr1.4.c to pr1.5.c
 *   general cleanup
 *   tested on Linux, Mac OS X, Solaris
 */

/* changes, pr1.3.c to pr1.4.c
 *   p2_actions() string matching, using the library function strstr()
 */

/* changes, pr1.2.c to pr1.3.c
 *   p1_actions() reads entire file
 *   simplify the parameters to p1_actions()
 *   p2_actions() reads entire pipe
 *     (no string matching yet)
 *   p3_actions() reads entire pipe
 *   simplify the parameters to p3_actions()
 */

/* changes, pr1.1.c to pr1.2.c
 *   command-line arguments in main()
 */

/* changes, pr1pipe.c to pr1.1.c
 *   rename parent_actions() p1_actions()
 *   rename child_actions()  p2_actions()
 *   add p3_actions()
 *   each should have two file descriptors as parameters, but it's easier to use
 *     FILE pointers sometimes
 * planned changes to main()
 *   read command line arguments
 *   open input and output files before fork()
 * changes to p1_actions()
 *   read from fd1 (file), write to fd2 (pipe)
 * changes to p2_actions()
 *   read from fd1 (pipe), write to fd2 (pipe)
 * changes to p1_actions()
 *   read from fd1 (pipe), write to fd2 (file)
 */

//--------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// This makes Solaris and Linux happy about waitpid(); it is not required on Mac OS X.
#include <sys/wait.h>

//--------------------------------------------------------------------------------

void err_sys(char *msg);        // print message and quit

struct match {  // used in p2_actions()
  int lines;  // number of matching input lines
  int matches;  // number of matches within lines
  char *string; // string to match against input lines
  int strlen; // strlen(string)
};

// In a pipe, the parent is upstream, and the child is downstream, connected by
//   a pair of open file descriptors.

void p1_actions(const char *i_file, FILE *fp1, int fd2);
void p2_actions(struct match *table, int fd1, int fd2);
void p3_actions(const char *o_file, int fd1, FILE *fp2);
  // fd = file descriptor, opened by open() or pipe()
  // treat fd as if it had come from open()
  // fp = FILE pointer, opened by fopen()
  // read from fp1 or fd1, write to fp2 or fd2

#define BUFFER_SIZE 4096

//--------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
  FILE *in, *out;   // input and output, using fopen()
  int pipe1[2];     // pipe endpoints
  pid_t p2_pid;     // child PID

  // for use with getopt()
  int ch;
  extern char *optarg;
  extern int optind;
  extern int optopt;
  extern int opterr;

  char *i_file = "/dev/null"; // default if no -i option supplied
  char *o_file = "/dev/null"; // default if no -o option supplied

  int table_count = 0;
  struct match *table = malloc(argc * sizeof(struct match));  // hold -m info
  if (table == NULL)
    {
      err_sys("malloc error (table)");
    }

  while ((ch = getopt(argc, argv, ":i:o:m:")) != -1)
    {
      switch (ch) {
  case 'i':   // only the last -i option is used
    i_file = optarg;
    break;
  case 'o':   // only the last -o option is used
    o_file = optarg;
    break;
  case 'm':   // save for p2_actions()
    table[table_count++] = (struct match){ 0, 0, optarg, strlen(optarg) };
    if (optarg[0] == '\0')
      { fprintf(stderr, "%s: invalid option '%c' (empty string)\n", argv[0], optopt);
        exit(1);
      }
    break;
  case '?':
    fprintf(stderr, "%s: invalid option '%c'\n", argv[0], optopt);
    exit(1);
    break;
  case ':':
    fprintf(stderr, "%s: invalid option '%c' (missing argument)\n", argv[0], optopt);
    exit(1);
    break;
  default:
    exit(1);
    break;
      }
    }

  table[table_count] = (struct match){ 0, 0, NULL, 0 }; // end-of-table marker

  if (strcmp(i_file, "-") == 0)
    {
      in = stdin;
    }
  else
    {
      in = fopen(i_file, "r");
      if (in == NULL)
  {
    err_sys("fopen error (in)");
  }
    }

  if (strcmp(o_file, "-") == 0)
    {
      out = stdout;
    }
  else
    {
      out = fopen(o_file, "w");
      if (out == NULL)
  {
    err_sys("fopen error (out)");
  }
    }

  if (pipe(pipe1) < 0)
    {
      err_sys("pipe error (1)");
    }

  if ((p2_pid = fork()) < 0)
    {
      err_sys("fork error (1)");
    }
  else if (p2_pid > 0)    // this is the parent (P1)
    {
      close(pipe1[0]);        // P2 will read from pipe1[0]
      p1_actions(i_file, in, pipe1[1]);   // read from in, write to pipe1[1]
      if (waitpid(p2_pid, NULL, 0) < 0)   // wait for child (P2)
  { err_sys("waitpid error (1)"); }
    }
  else        // this is the child (P2)
    {
      close(pipe1[1]);        // P1 will write to pipe1[1]

      // copy the setup using pipe() and fork(), renaming pipe1, P1, P2 to pipe2, P2, P3
      int pipe2[2];   // pipe endpoints
      pid_t p3_pid;   // child PID

      if (pipe(pipe2) < 0)
  {
    err_sys("pipe error (2)");
  }

      if ((p3_pid = fork()) < 0)
  {
    err_sys("fork error (2)");
  }
      else if (p3_pid > 0)  // this is the parent (P2)
  {
    close(pipe2[0]);      // P3 will read from pipe2[0]
    p2_actions(table, pipe1[0], pipe2[1]);
      // read from pipe1[0], write to pipe2[1]
    if (waitpid(p3_pid, NULL, 0) < 0) // wait for child (P3)
      { err_sys("waitpid error (2)"); }
  }
      else      // this is the child (P3)
  {
    close(pipe2[1]);      // P2 will write to pipe2[1]
    p3_actions(o_file, pipe2[0], out);  // read from pipe2[0]
  }
      // end of copied-and-modified part
    }

  return 0;
}

//--------------------------------------------------------------------------------

// print message and quit

void err_sys(char *msg)
{
  printf("error: PID %d, %s\n", getpid(), msg);
  exit(0);
}

//--------------------------------------------------------------------------------

// read from fp1, write to fd2

void p1_actions(const char *i_file, FILE *fp1, int fd2)
{
  FILE *fp2 = fdopen(fd2, "w");   // use fp2 as if it had come from fopen()
  if (fp2 == NULL)
    { err_sys("fdopen(w) error (p1)"); }

  static char buffer[BUFFER_SIZE];  // off the stack, always allocated
  int ret = setvbuf(fp2, buffer, _IOLBF, BUFFER_SIZE);  // set fp2 to line-buffering
  if (ret != 0)
    { err_sys("setvbuf error (p1)"); }

  // read bytes from fp1, write to fp2

  int count = 0;
  for (int c = getc(fp1); c != EOF; c = getc(fp1))
    { count++; putc(c, fp2); }

  // cleanup
  fclose(fp1);
  fclose(fp2);

  // final tally
  printf("P1: file %s, bytes %d\n", i_file, count);
}

//--------------------------------------------------------------------------------

// read from fd1, write to fd2

void p2_actions(struct match *table, int fd1, int fd2)
{
  FILE *fp1 = fdopen(fd1, "r");
  if (fp1 == NULL)
    { err_sys("fdopen(r) error (2)"); }

  FILE *fp2 = fdopen(fd2, "w");
  if (fp2 == NULL)
    { err_sys("fdopen(w) error (2)"); }

  static char buffer1[BUFFER_SIZE];
  int ret = setvbuf(fp1, buffer1, _IOLBF, BUFFER_SIZE);
  if (ret != 0)
    { err_sys("setvbuf error (p2, 1)"); }

  static char buffer2[BUFFER_SIZE];
  ret = setvbuf(fp2, buffer2, _IOLBF, BUFFER_SIZE);
  if (ret != 0)
    { err_sys("setvbuf error (p2, 2)"); }

  char line[BUFFER_SIZE];

  for (char *p = fgets(line, BUFFER_SIZE, fp1); // ends with newline and null_char
     p != NULL;
     p = fgets(line, BUFFER_SIZE, fp1))
  {
    int matched_this_line = 0;
    for (int i = 0; table[i].string != NULL; i++) {
      char *q = strstr(p, table[i].string);

      if (q != NULL) {
        matched_this_line++;
        table[i].lines++;

        for (char *r = q;
          r != NULL;
          r = strstr(r + table[i].strlen, table[i].string))
        {
          table[i].matches++;
        }
      }
    }
    if (matched_this_line > 0) fprintf(fp2, "%s", p);
  }

  // cleanup
  fclose(fp1);
  fclose(fp2);

  // final tally
  for (int i = 0; table[i].string != NULL; i++) {
    printf("P2: string %s, lines %d, matches %d\n",
    table[i].string, table[i].lines, table[i].matches);
  }
}

//--------------------------------------------------------------------------------

// read from fd1, write to fp2

void p3_actions(const char *o_file, int fd1, FILE *fp2)
{
  FILE *fp1 = fdopen(fd1, "r");
  if (fp1 == NULL)
    { err_sys("fdopen(r) error (3)"); }

  static char buffer1[BUFFER_SIZE];
  int ret = setvbuf(fp1, buffer1, _IOLBF, BUFFER_SIZE);
  if (ret != 0)
    { err_sys("setvbuf error (p3, 1)"); }

  char line[BUFFER_SIZE];

  int count = 0;
  for (char *p = fgets(line, BUFFER_SIZE, fp1); // ends with newline and null_char
       p != NULL;
       p = fgets(line, BUFFER_SIZE, fp1))
    {
      count++; fprintf(fp2, "%s", p);
    }

  // cleanup
  fclose(fp1);
  fclose(fp2);

  // final tally
  printf("P3: file %s, lines %d\n", o_file, count);
}

//--------------------------------------------------------------------------------


