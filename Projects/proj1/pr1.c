/* CMPSC 473, Project 1
 *
 * FILE:            pr1.c
 * AUTHOR:          Ted Papaioannou
 * EMAIL:           tap5199@psu.edu
 * PSU ACCESS ID:   984238848
 * SUBMITTED ON:    02/06/2014
 */

//------------------------------------------------------------------------------

// Includes
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
// This makes Solaris and Linux happy about waitpid(); it is not required on Mac OS X.
#include <sys/wait.h>

// Defines
#define BUFFER_SIZE 4096
#define ARGUMENTS "i:o:m:"
#define DEFAULT_INPUT "/dev/null"
#define DEFAULT_OUTPUT "/dev/null"

// Typedefs
typedef struct {
    char* string;
    int lines;
    int matches;
} Matcher;

// Functional prototypes
void p1_actions(int fd);                // write to fd
void p2_actions(int fdr, int fdw);      // read from fdr, write to fdw
void p3_actions(int fd);                // read from fd
void cleanUp();                         // cleans up allocated memory
void err_sys(char *msg);                // print message and quit
int parseArgs(int argc, char* argv[]);  // parses the command line arguments
int match(Matcher* match, char* line);  // Matches bases off of -m args
Matcher* newMatcher(char* string);      // allocates a new Matcher object
void printMatcher(Matcher* match);      // wrapper for printing Matcher struct

// Global Variables
struct arg_struct { // Holds parsed arguments and meta data about them
  char* inFile;     // Input file to read from
  char* outFile;    // Output file to write to
  int mArgc;        // Count of -m arguments
  char** mArgv;     // Array of -m arguments
} arguments;

////////////////////////////////////////////////////////////////////////////////
//
// Function     : main
// Description  : The main function for the Project 1
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int main(int argc, char *argv[]) {

  int fd12[2];        // pipe from p1 to p2 endpoints
  int fd23[2];        // pipe from p2 to p3 endpoints
  pid_t child1_pid, child2_pid;

  // Parse the command line arguments into the global arguments struct
  if (parseArgs(argc, argv) < 0) {
    err_sys("argument parsing error");
  }

  // Create pipe from p1 to p2
  if (pipe(fd12) < 0) {
    err_sys("pipe 1 to 2 error");
  }
  // Create pipe from p2 to p3
  if (pipe(fd23) < 0) {
    err_sys("pipe 2 to 3 error");
  }

  // Create a child process for p2
  if ((child1_pid = fork()) < 0) {
    err_sys("fork error");
  }
  else if (child1_pid > 0) {  // this is p1 and the parent
    close(fd23[0]);       // Unused pipe
    close(fd23[1]);       // Unused pipe
    close(fd12[0]);       // read from fd[0]
    p1_actions(fd12[1]);  // write to fd[1]

    if (waitpid(child1_pid, NULL, 0) < 0) {  // wait for child
      err_sys("waitpid 1 error");
    }
  }

  else {  // this is p2
    // Create a child process for p3
    if ((child2_pid = fork()) < 0) {
      err_sys("fork error");
    }
    else if (child2_pid > 0) {  // this is p2 and the parent of p3
      close(fd12[1]);                 // write to fd12[1]
      close(fd23[0]);                 // read from fd23[0]
      p2_actions(fd12[0], fd23[1]);   // read from fd12[0], write to fd23[1]

      if (waitpid(child2_pid, NULL, 0) < 0) {  // wait for child
        err_sys("waitpid 2 error");
      }
    }
    else { // this is p3
      close(fd12[0]);       // Unused pipe
      close(fd12[1]);       // Unused pipe
      close(fd23[1]);       // write to fd23[1]
      p3_actions(fd23[0]);  // read from fd23[0]
    }
  }

  cleanUp();
  return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p1_actions
// Description  : read from file, write to fd
//
// Inputs       : fd - the file descriptor to write to
// Outputs      : None

void p1_actions(int fd) {

  char line[BUFFER_SIZE];
  char* p;
  int bytesRead = 0;

  // Check if input file was supplied
  if (arguments.inFile == NULL) {
     err_sys("No input file supplied");
  }

  // Open the input file
  FILE* in = fopen(arguments.inFile, "r");
  if (in == NULL) {
    err_sys("fopen(r) error");
  }

  // Open the output file for the pipe
  FILE* fp = fdopen(fd, "w");     // use fp as if it had come from fopen()
  if (fp == NULL) {
    err_sys("fdopen(w) error");
  }

  // The following is so we don't need to call fflush(fp) after each fprintf().
  // The default for a pipe-opened stream is full buffering, so we switch to line
  //   buffering.
  // But, we need to be careful not to exceed BUFFER_SIZE characters per output
  //   line, including the newline and null terminator.
  static char buffer[BUFFER_SIZE];  // off the stack, always allocated
  int ret = setvbuf(fp, buffer, _IOLBF, BUFFER_SIZE); // set fp to line-buffering
  if (ret != 0) {
    err_sys("setvbuf error (p1)");
  }

  // Put each line of the file onto the pipe
  while ((p = fgets(line, BUFFER_SIZE, in)) != NULL) {
    bytesRead += strlen(p);
    fprintf(fp, "%s", line);
  }

  // Print the results
  printf("P1: file %s, bytes %d\n", arguments.inFile, bytesRead);

  // Clean up the file stream and the file descriptor
  fclose(fp);
  close(fd);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p2_actions
// Description  : read from fdr, write to fdw
//
// Inputs       : fdr - the file descriptor to read from
//                fdw - the file descriptor to write to
// Outputs      : None

void p2_actions(int fdr, int fdw) {

  int i;
  //int bytesRead = 0;
  Matcher* matchers[arguments.mArgc]; // An array of the Matchers objects

  // see parent_actions() for similar code, with comments
  FILE *fpr = fdopen(fdr, "r");
  if (fpr == NULL) {
    err_sys("fdopen(r) error");
  }

  FILE *fpw = fdopen(fdw, "w");
  if (fpw == NULL) {
    err_sys("fdopen(r) error");
  }

  static char buffer[BUFFER_SIZE];
  int retr = setvbuf(fpr, buffer, _IOLBF, BUFFER_SIZE);
  int retw = setvbuf(fpw, buffer, _IOLBF, BUFFER_SIZE);
  if ((retr != 0) && (retw != 0)) {
    err_sys("setvbuf error (p2)");
  }

  char line[BUFFER_SIZE];
  char *p;

  // Create the array of matchers
  for (i = 0; i < arguments.mArgc; i++) {
    matchers[i] = newMatcher(arguments.mArgv[i]);
  }

  // Match all the lines coming in from the pipe
  while ((p = fgets(line, BUFFER_SIZE, fpr)) != NULL) {
    // Loop through all the matchers to check this line
    int matchCount = 0;
    for (i = 0; i < arguments.mArgc; i++) {
      if (match(matchers[i], line) > -1) {
        matchCount++;
      }
    }
    if (matchCount > 0) {
      fprintf(fpw, "%s", line); // Send to pipe if the line had any matches
    }
  }

  // Print all the matches
  for (i = 0; i < arguments.mArgc; i++) {
    if (matchers[i]->matches != 0) {
      printf("P2: ");
      printMatcher(matchers[i]);
      printf("\n");
    }
  }

  // Clean up the array of Matchers
  for (i = 0; i < arguments.mArgc; i++) {
    free(matchers[i]);
    matchers[i] = NULL;
  }

  // Clean up the file streams and file handles
  fclose(fpr);
  fclose(fpw);
  close(fdr);
  close(fdw);

}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : p3_actions
// Description  : read from fd
//
// Inputs       : fd - the file descriptor to read from
// Outputs      : None

void p3_actions(int fd) {

  // see parent_actions() for similar code, with comments
  FILE *fp = fdopen(fd, "r");
  if (fp == NULL) {
    err_sys("fdopen(r) error");
  }

  // Open the output file
  FILE* out = fopen(arguments.outFile, "w");
  if (out == NULL) {
    out = stdout;
  }

  static char buffer[BUFFER_SIZE];
  int ret = setvbuf(fp, buffer, _IOLBF, BUFFER_SIZE);
  if (ret != 0) {
    err_sys("setvbuf error (child)");
  }

  char line[BUFFER_SIZE];

  char *p;
  int lineCount = 0;
  while ((p = fgets(line, BUFFER_SIZE, fp)) != NULL) {
    fprintf(out, "%s", line);
    lineCount++;
  }

  // Print the results
  printf("P3: file %s lines %d\n", arguments.outFile, lineCount);

  // Clean up the file streams and file handles
  fclose(fp);
  fclose(out);
  close(fd);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : cleanUp
// Description  : Cleans up the allocated memory
//
// Inputs       : None
// Outputs      : None

void cleanUp() {
  free(arguments.mArgv);
  arguments.mArgv = NULL;
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : err_sys
// Description  : print message and quit
//
// Inputs       : msg - the error message
// Outputs      : None

void err_sys(char *msg) {
  printf("error: PID %d, %s\n", getpid(), msg);
  exit(0);
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : parseArgs
// Description  : parse the command line arguments into the global arguments
//                struct
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int parseArgs(int argc, char* argv[]) {
  int ch;

  // Initialize the arguments
  arguments.inFile = NULL;
  arguments.outFile = NULL;
  arguments.mArgc = 0;

  // Collect the input file, output file, and count of -m args
  while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
    switch(ch) {
    case 'i': // Set input file
      arguments.inFile = optarg;
      break;

    case 'm': // Count the -m args
      arguments.mArgc++;
      break;

    case 'o': // Set output file
      arguments.outFile = optarg;
      break;

    default:
      break;
    }
  }

  // Check only the input file, output defaults to stdin
  if (arguments.inFile == NULL) {
    arguments.inFile = DEFAULT_INPUT;
  }
  if (arguments.outFile == NULL) {
    arguments.outFile = DEFAULT_OUTPUT;
  }

  // Check if any -m arguments were supplied
  if (arguments.mArgc == 0) {
    fprintf(stderr, "warning: no -m option was supplied\n");
  }
  else {
    // Loop through again and collect the -m arguments
    optind = 1; // Reset the index for getopt
    arguments.mArgv = malloc(arguments.mArgc * sizeof(char*));
    int mInd = 0;
    while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
      if (ch == 'm') {
        arguments.mArgv[mInd++] = optarg;
      }
    }
  }

  return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : match
// Description  : match a line against all of the -m argument Matcher struct
//                supplied increments the match count and line count in the
//                struct if necessary.
//
// Inputs       : match - the match struct for 1 -m argument
// Outputs      : 0 if success, -1 if failure

int match(Matcher* match, char* line) {

  int count = 0;

  // strstr(char*, char*) returns a pointer to the beginning of the first
  // occurences of the string to search for. Therefore, we must move the start
  // of the pointer pass this point, after each call of strstr, to find the next
  // occurence.
  char* copy = strdup(line);
  while((copy = strstr(copy, match->string)) != NULL) {
    copy += strlen(match->string);
    count++;
    match->matches++;
  }

  // Commit the results
  if (count == 0) {
    return( -1 );
  }

  match->lines++;           // A line has been matched

  return( 0 );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : newMatcher
// Description  : instantiates a new Matcher object
//
// Inputs       : string - the -m argument
// Outputs      : the new Matcher object

Matcher* newMatcher(char* string) {
  Matcher* match = malloc(sizeof(Matcher));
  match->string = string;
  match->lines = 0;
  match->matches = 0;

  return( match );
}

////////////////////////////////////////////////////////////////////////////////
//
// Function     : printMatcher
// Description  : wrapper function for printing a Matcher struct
//
// Inputs       : fd - the file descriptor to read from
// Outputs      : None

void printMatcher(Matcher* match) {
  printf("string %s, lines %d, matches %d", match->string, match->lines, match->matches);
}

//--------------------------------------------------------------------------------
