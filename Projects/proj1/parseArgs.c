/* CMPSC 473, Project 1
 *
 * FILE:            pr1.c
 * AUTHOR:          Ted Papaioannou
 * EMAIL:           tap5199@psu.edu
 * PSU ACCESS ID:   984238848
 */

//--------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// This makes Solaris and Linux happy about waitpid(); it is not required on Mac OS X.
#include <sys/wait.h>

//--------------------------------------------------------------------------------

void err_sys(char *msg);        // print message and quit
int parseArgs(int argc, char* argv[]);

#define BUFFER_SIZE 4096
#define ARGUMENTS "i:o:m:"
#define DEFAULT_OUTPUT "default-output.txt"

// Global Variables
struct arg_struct {
  char* inFile;
  char* outFile;
  int mArgc;
  char** mArgv;
} arguments;

////////////////////////////////////////////////////////////////////////////////
//
// Function     : main
// Description  : The main function for the Project 1
//
// Inputs       : argc - the number of command line parameters
//                argv - the parameters
// Outputs      : 0 if successful, -1 if failure

int main(int argc, char *argv[]) {

  // Parse the command line arguments into the global arguments struct
  if (parseArgs(argc, argv) < 0) {
    err_sys("argument parsing error");
  }

  printf("Arguments:\n\n");

  printf("input: %s, output: %s\n", arguments.inFile, arguments.outFile);
  int i;
  for (i = 0; i < arguments.mArgc; i++) {
    printf("mArgv[%d] = %s\n", i, arguments.mArgv[i]);
  }
  return 0;
}

void err_sys(char *msg) {
  printf("error: PID %d, %s\n", getpid(), msg);
  exit(0);
}
int parseArgs(int argc, char* argv[]) {
  int ch;

  // Initialize the arguments
  arguments.inFile = NULL;
  arguments.outFile = NULL;
  arguments.mArgc = 0;

  // Collect the input file, output file, and count of -m args
  while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
    switch(ch) {
    case 'i': // Set input file
      arguments.inFile = optarg;
      break;

    case 'm': // Count the -m args
      arguments.mArgc++;
      break;

    case 'o': // Set output file
      arguments.outFile = optarg;
      break;

    default:
      break;
    }
  }

  if ((arguments.inFile == NULL) || (arguments.outFile == NULL)) {
    return( -1 );
  }

  // Loop through again and collect the -m arguments
  optind = 1; // Reset the index for getopt
  arguments.mArgv = malloc(arguments.mArgc * sizeof(char*));
  int mInd = 0;
  while ((ch = getopt(argc, argv, ARGUMENTS)) != -1) {
    if (ch == 'm') {
      arguments.mArgv[mInd++] = optarg;
    }
  }

  static char test[50];
  int cnt = sprintf(test, "hi + %d\t", 2031);
  printf("%s : %d\n", test, cnt);

  return( 0 );
}
//--------------------------------------------------------------------------------

